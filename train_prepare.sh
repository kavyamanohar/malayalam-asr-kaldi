#set-up for single machine or cluster based execution
. ./cmd.sh
#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh

kaldi_root_dir='/home/kavya/kavyadev/kaldi'
basepath='.'

Create fresh data files
mkdir data
mkdir data/trainAM


for folder in openslr indictts
do

echo ============================================================================
echo "                  Preparing Data Files for Featue extraction from $folder	        "
echo ============================================================================

#Read path of wave files and store it as a temporary file wavefilepaths.txt 
realpath ./raw/waves/$folder/*.wav >> ./data/trainAM/wavefilepaths.txt


echo "Creating the list of utterence IDs"

#The function is to extract the utterance id
cat ./data/trainAM/wavefilepaths.txt | xargs -l basename -s .wav > ./data/trainAM/utt


echo "Creating the list of utterence IDs mapped to absolute file paths of wavefiles"


#Create wav.scp mapping from uttrence id to absolute wave file paths
paste ./data/trainAM/utt ./data/trainAM/wavefilepaths.txt > ./data/trainAM/wav.scp


echo "Creating the list of speaker IDs"
cat ./raw/wavdata/$folder/utt2spk >> ./data/trainAM/utt2spk


echo "     Preparing transcripts    	        "

echo "Creating the text file of uttid mapped to transcript separated by tab in sorted order"
sort ./raw/wavdata/$folder/transcript.tsv >> ./data/trainAM/text

done

./utils/utt2spk_to_spk2utt.pl ./data/trainAM/utt2spk > ./data/trainAM/spk2utt

rm ./data/trainAM/wavefilepaths.txt

echo ============================================================================
echo "     Fixing data directories    	        "
echo ============================================================================
./utils/fix_data_dir.sh ./data/trainAM

echo ============================================================================
echo "     MFCC Feature Extraction and Mean-Variance Tuning Files for trainAM    	        "
echo ============================================================================


#Create feature vectors
./steps/make_mfcc.sh --nj 6 data/trainAM exp/make_mfcc/trainAM mfcc

#Copy the feature in text file formats for human reading
copy-feats ark:./mfcc/raw_mfcc_trainAM.1.ark ark,t:./mfcc/raw_mfcc_trainAM.1.txt


#Create Mean Variance Tuning
steps/compute_cmvn_stats.sh data/trainAM exp/make_mfcc/trainAM mfcc


echo ============================================================================
echo "                  Preparing the Unified Parser Lexicon Dictionary       	        "
echo ============================================================================
train_lang=lang_model_up
train_dict=dict_up

mkdir -p data/local/$train_dict

echo "Creating the sorted lexicon file"
sort ./raw/language/lexicon_up.txt | paste > ./data/local/$train_dict/lexicon.txt 


echo "Creating the list of Phones"
cat ./data/local/$train_dict/lexicon.txt | cut -d '	' -f 2  - | tr ' ' '\n' | sort | uniq > ./data/local/$train_dict/phones.txt 
sed -i '/^$/d ' ./data/local/$train_dict/phones.txt #Delete blank lines form phones.txt

cat ./data/local/$train_dict/phones.txt | sed /SIL/d | sed /SPN/d > ./data/local/$train_dict/nonsilence_phones.txt 

echo "SIL" > ./data/local/$train_dict/optional_silence.txt 
echo "SIL" > ./data/local/$train_dict/silence_phones.txt
echo "SPN" >> ./data/local/$train_dict/silence_phones.txt

touch ./data/local/$train_dict/extra_phones.txt ./data/local/$train_dict/extra_questions.txt

echo ============================================================================
echo "                   Creating the Unified Parser lexicon dictionary L.fst               	        "
echo ============================================================================

mkdir $basepath/data/local/tmp_$train_lang

utils/prepare_lang.sh --num-sil-states 3 data/local/$train_dict "<unk>" data/local/$train_lang data/$train_lang


echo ============================================================================
echo "                  Preparing the mlphon Lexicon Dictionary       	        "
echo ============================================================================
train_lang=lang_model_mlphon
train_dict=dict_mlphon

mkdir -p data/local/$train_dict

echo "Creating the sorted lexicon file"
sort ./raw/language/lexicon_mlphon.txt | paste > ./data/local/$train_dict/lexicon.txt 


echo "Creating the list of Phones"
cat ./data/local/$train_dict/lexicon.txt | cut -d '	' -f 2  - | tr ' ' '\n' | sort | uniq > ./data/local/$train_dict/phones.txt 
sed -i '/^$/d ' ./data/local/$train_dict/phones.txt #Delete blank lines form phones.txt

cat ./data/local/$train_dict/phones.txt | sed /SIL/d | sed /SPN/d > ./data/local/$train_dict/nonsilence_phones.txt 

echo "SIL" > ./data/local/$train_dict/optional_silence.txt 
echo "SIL" > ./data/local/$train_dict/silence_phones.txt
echo "SPN" >> ./data/local/$train_dict/silence_phones.txt

touch ./data/local/$train_dict/extra_phones.txt ./data/local/$train_dict/extra_questions.txt

echo ============================================================================
echo "                   Creating the mlphon lexicon dictionary L.fst               	        "
echo ============================================================================

mkdir $basepath/data/local/tmp_$train_lang

utils/prepare_lang.sh --num-sil-states 3 data/local/$train_dict "<unk>" data/local/$train_lang data/$train_lang

echo ============================================================================
echo "                   Preparing for LM training               	        "
echo ============================================================================

cat ./raw/language/lm_train.txt > ./data/trainAM/lm_train.txt

echo ============================================================================
echo "                   Creating  n-gram LM G.fst in Unified parser directory           	        "
echo ============================================================================
train_lang=lang_model_up
train_folder=trainAM
n_gram=2 # This specifies bigram or trigram. for bigram set n_gram=2 for tri_gram set n_gram=3



$kaldi_root_dir/tools/irstlm/bin/build-lm.sh -i $basepath/data/$train_folder/lm_train.txt -n $n_gram -o $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | utils/find_arpa_oovs.pl data/$train_lang/words.txt  > data/local/tmp_$train_lang/oov.txt

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | grep -v '<s> <s>' | grep -v '<s> </s>' | grep -v '</s> </s>' | grep -v 'SIL' | $kaldi_root_dir/src/lmbin/arpa2fst - | fstprint | utils/remove_oovs.pl data/local/tmp_$train_lang/oov.txt | utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=data/$train_lang/words.txt --osymbols=data/$train_lang/words.txt --keep_isymbols=false --keep_osymbols=false | fstrmepsilon > data/$train_lang/G.fst 
$kaldi_root_dir/src/fstbin/fstisstochastic data/$train_lang/G.fst 

echo ============================================================================
echo "                   Creating  n-gram LM G.fst  in mlphon directory          	        "
echo ============================================================================
train_lang=lang_model_mlphon
train_folder=trainAM
n_gram=2 # This specifies bigram or trigram. for bigram set n_gram=2 for tri_gram set n_gram=3



$kaldi_root_dir/tools/irstlm/bin/build-lm.sh -i $basepath/data/$train_folder/lm_train.txt -n $n_gram -o $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | utils/find_arpa_oovs.pl data/$train_lang/words.txt  > data/local/tmp_$train_lang/oov.txt

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | grep -v '<s> <s>' | grep -v '<s> </s>' | grep -v '</s> </s>' | grep -v 'SIL' | $kaldi_root_dir/src/lmbin/arpa2fst - | fstprint | utils/remove_oovs.pl data/local/tmp_$train_lang/oov.txt | utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=data/$train_lang/words.txt --osymbols=data/$train_lang/words.txt --keep_isymbols=false --keep_osymbols=false | fstrmepsilon > data/$train_lang/G.fst 
$kaldi_root_dir/src/fstbin/fstisstochastic data/$train_lang/G.fst 



echo ============================================================================
echo "     Fixing data directories    	        "
echo ============================================================================
./utils/fix_data_dir.sh ./data/trainAM


echo ============================================================================
echo "                   End of Script             	        "
echo ============================================================================
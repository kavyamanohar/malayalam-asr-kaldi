FROM kaldiasr/kaldi:latest
MAINTAINER Kavya "sakhi.kavya@gmail.com"
ENV KALDI_HOME /opt/kaldi
WORKDIR $KALDI_HOME
RUN apt install -y python3-pip
RUN pip3 install flask
RUN mkdir ./egs/ml_asr/
ENV KALDI_HOME /opt/kaldi/egs/ml_asr
WORKDIR $KALDI_HOME
RUN mkdir ./data/ 
RUN mkdir ./exp/ 
COPY ./conf ./conf
COPY ./steps ./steps
COPY ./utils ./utils
COPY ./data/lang_bigram ./data/lang_bigram/
COPY ./exp/tri_lda ./exp/tri_lda/
COPY ./exp/DNN_tri_lda_aligned_layer3_nodes256 ./exp/DNN_tri_lda_aligned_layer3_nodes256/
COPY ./static ./static/
COPY ./templates ./templates/
COPY ./uploads ./uploads/
COPY ./path.sh ./
COPY ./speech2text.sh ./
COPY ./quick-asr.sh ./
COPY ./server.py ./
COPY ./example.wav ./
EXPOSE 8080
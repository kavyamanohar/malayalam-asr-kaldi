#set-up for single machine or cluster based execution
. ./cmd.sh
#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh


# echo ============================================================================
# echo "          Preparing Data Files for Language Modeling  	        "
# echo ============================================================================

# for folder in codemix
# do

# mkdir ./data/$folder


# echo "Creating the text file of uttid mapped to transcript separated by tab in sorted order for $folder"
# cat ./raw/wavdata/codemix/trans > ./data/$folder/trans

# done

# for folder in codemix
# do

# echo "Expanding LM model creation input file from  $folder"
# while read line
# do
# echo "<s> $line </s>" >> ./data/trainAM/lm_train.txt
# done <./data/$folder/trans
# done

# echo ============================================================================
# echo "                  Preparing the Lexicon Dictionary       	        "
# echo ============================================================================

# mkdir -p data/local/dict_mlphon

# echo "Creating the sorted lexicon file"
# sort ./raw/language/uplexicon_train2.txt | paste > ./data/local/dict/lexicon.txt 


# Creating the lexicon with extended wordlist
# cat ./data/local/dict_mlphon/lexicon.txt >  ./data/local/dict_mlphon/temporarylexicon
# sort ./raw/wavdata/codemix/lexicon.txt | paste >>  ./data/local/dict_mlphon/temporarylexicon
# sort -u ./data/local/dict_mlphon/temporarylexicon > ./data/local/dict_mlphon/lexicon.txt 
# rm ./data/local/dict_mlphon/temporarylexicon

echo "Creating the list of Phones"
cat ./data/local/dict_mlphon/lexicon.txt | cut -d '	' -f 2  - | tr ' ' '\n' | sort | uniq > ./data/local/dict_mlphon/phones.txt 
sed -i '/^$/d ' ./data/local/dict_mlphon/phones.txt #Delete blank lines form phones.txt

cat ./data/local/dict_mlphon/phones.txt | sed /SIL/d | sed /SPN/d > ./data/local/dict_mlphon/nonsilence_phones.txt 

# echo "SIL" > ./data/local/dict_mlphon/optional_silence.txt 
# echo "SIL" > ./data/local/dict_mlphon/silence_phones.txt
# echo "SPN" >> ./data/local/dict_mlphon/silence_phones.txt

# touch ./data/local/dict_mlphon/extra_phones.txt ./data/local/dict_mlphon/extra_questions.txt




# echo ============================================================================
# echo "                  Preparing the Language Model Files    	        "
# echo ============================================================================

# #kaldi_root_dir='../..'
kaldi_root_dir='/home/kavya/kavyadev/kaldi'
basepath='.'



train_dict=dict_mlphon
train_lang=lang_model_mlphon
train_folder=trainAM

n_gram=3 # This specifies bigram or trigram. for bigram set n_gram=2 for tri_gram set n_gram=3

echo ============================================================================
echo "                   Creating  lexicon dictionary L.fst               	        "
echo ============================================================================

rm -rf $basepath/data/local/$train_dict/lexiconp.txt $basepath/data/local/$train_lang $basepath/data/local/tmp_$train_lang $basepath/data/$train_lang

mkdir $basepath/data/local/tmp_$train_lang

utils/prepare_lang.sh --num-sil-states 3 data/local/$train_dict "<unk>" data/local/$train_lang data/$train_lang


echo ============================================================================
echo "                   Creating  n-gram LM G.fst           	        "
echo ============================================================================

$kaldi_root_dir/tools/irstlm/bin/build-lm.sh -i $basepath/data/$train_folder/syl_lm_train.txt -n $n_gram -o $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | utils/find_arpa_oovs.pl data/$train_lang/words.txt  > data/local/tmp_$train_lang/oov.txt

gunzip -c $basepath/data/local/tmp_$train_lang/lm_phone_bg.ilm.gz | grep -v '<s> <s>' | grep -v '<s> </s>' | grep -v '</s> </s>' | grep -v 'SIL' | $kaldi_root_dir/src/lmbin/arpa2fst - | fstprint | utils/remove_oovs.pl data/local/tmp_$train_lang/oov.txt | utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=data/$train_lang/words.txt --osymbols=data/$train_lang/words.txt --keep_isymbols=false --keep_osymbols=false | fstrmepsilon > data/$train_lang/G.fst 
$kaldi_root_dir/src/fstbin/fstisstochastic data/$train_lang/G.fst 



echo ============================================================================
echo "                   Fixing Train and Test Data directories               	        "
echo ============================================================================

./utils/fix_data_dir.sh $basepath/data/$train_folder

echo ============================================================================
echo "                   End of Script             	        "
echo ============================================================================
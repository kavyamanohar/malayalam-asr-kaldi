## Use the code below to get word lattice. Give utterance id, lattice in gzip format and graph/words.txt as inputs. You can get the lattice in svg or pdf format

`utils/show_lattice.sh k1 inputaudio/transcriptions/lat.gz exp/tri_lda/graph/words.txt` 

## To get lat.gz from lattice.ark use

`gzip -c lattice.ark > lat.gz`

Or use lat.gz from exp/modelname/decode/lat.gz


### Reference

http://jrmeyer.github.io/asr/2016/12/15/Visualize-lattice-kaldi.html

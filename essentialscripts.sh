#!/bin/bash
#############################Clean up Transcript of Open SLR corpora############
# filename='./raw/text/iiith/transcript.tsv'
# sed -i 's/ന്‍/ൻ/g' $filename
# sed -i 's/ള്‍/ൾ/g' $filename
# sed -i 's/ല്‍/ൽ/g' $filename
# sed -i 's/ര്‍/ർ/g' $filename
# sed -i 's/ണ്‍/ൺ/g' $filename
# sed -i 's/ൻറെ/ന്റെ/g' $filename
# # Remove ZWNJ at end of words
# sed -i 's/\xE2\x80\x8C//g' $filename
# # Remove all other ZWJ
# sed -i 's/\xE2\x80\x8D//g' $filename
# # Remove all soft hyphens
# sed -i 's/\xC2\xAD//g' $filename
# # Replace old au sign with new one
# sed -i 's/‍ൌ/ൗ/g' $filename


#############################Clean up Transcript of IITM TTS corpora############
# filename='./raw/text/train1/transcript.tsv'
# sort $filename -o $filename
# sed -i 's/ന്‍/ൻ/g' $filename
# sed -i 's/ള്‍/ൾ/g' $filename
# sed -i 's/ല്‍/ൽ/g' $filename
# sed -i 's/ര്‍/ർ/g' $filename
# sed -i 's/ണ്‍/ൺ/g' $filename
# sed -i 's/ൻറെ/ന്റെ/g' $filename
# # Remove ZWNJ at end of words
# sed -i 's/\xE2\x80\x8C//g' $filename
# # Remove all other ZWJ
# sed -i 's/\xE2\x80\x8D//g' $filename
# # Remove all soft hyphens
# sed -i 's/\xC2\xAD//g' $filename
# # Replace old au sign with new one
# sed -i 's/‍ൌ/ൗ/g' $filename
# sed -i 's/?//g' $filename # Removing special characters from word list(?)
# sed -i 's/,//g' $filename # Removing (,)
# sed -i 's/!//g' $filename # Removing (!)

##########WORDLIST CLEANUP (IITM TTS corpora)###############
# sort ./raw/text/train1/transcript.tsv -o ./raw/text/train1/transcript.tsv
# awk '{print $2}' FS='\t' ./raw/text/test1/test_filt.txt > ./raw/text/test1/trans
# awk '{print $1}' FS='\t' ./raw/text/train1/transcript.tsv > ./raw/text/train1/utt
# sed -i 's/[a-z]//g' ./raw/text/train1/trans
# sed -i 's/[A-Z]//g' ./raw/text/train1/trans
# sed -i 's/\-//g' ./raw/text/train1/trans
# sed -i 's/\_//g' ./raw/text/train1/trans
# sed -i 's/?//g' ./raw/text/train1/trans # Removing special characters from word list(?)
# sed -i 's/,//g' ./raw/text/train1/trans # Removing (,)
# sed -i 's/!//g' ./raw/text/train1/trans # Removing (!)
# sed -i 's/\.//g ' ./raw/text/train1/trans # Removing (.)
# paste ./raw/text/train1/utt ./raw/text/train1/trans > ./raw/text/train1/transcript.tsv
# tr ' ' '\n' < ./raw/text/train1/trans | sort | uniq > ./raw/text/train1/words_train1.txt # Find all unique words and write it in .word_tmp.txt
# sed -i '/^$/d'  ./raw/text/train1/words_tmp.txt # Deleting blank lines
# tr ' ' '\n' < ./raw/text/test1/trans | sort | uniq > ./raw/text/test1/words_test1.txt # Find all unique words and write it in .word_tmp.txt
# sort -u -o ./raw/text/test1/trans ./raw/text/test1/words_test1.txt
# tr ' ' '\n' < ./raw/text/test1/trans | sort  | uniq -c  > ./raw/text/test1/words_test1count.txt
# #(Manually delete the blank line just before eof)

##########WORDLIST CLEANUP (OPEN SLR)###############
# sort ./raw/text/train2/transcript.tsv -o ./raw/text/train2/transcript.tsv
# awk '{print $2}' FS='\t' ./raw/text/train2/transcript.tsv > ./raw/text/train2/trans
# awk '{print $1}' FS='\t' ./raw/text/train2/transcript.tsv > ./raw/text/train2/utt
# sed -i 's/[a-z]//g' ./raw/text/train2/trans
# sed -i 's/[A-Z]//g' ./raw/text/train2/trans
# sed -i 's/\-//g' ./raw/text/train2/trans
# sed -i 's/\_//g' ./raw/text/train2/trans
# sed -i 's/?//g' ./raw/text/train2/trans # Removing special characters from word list(?)
# sed -i 's/,//g' ./raw/text/train2/trans # Removing (,)
# sed -i 's/!//g' ./raw/text/train2/trans # Removing (!)
# sed -i 's/\.//g ' ./raw/text/train2/trans # Removing (.)
# paste ./raw/text/train2/utt ./raw/text/train2/trans > ./raw/text/train2/transcript.tsv
# tr ' ' '\n' < ./raw/text/train2/trans | sort | uniq > ./raw/text/train2/words_train2.txt # Find all unique words and write it in .word_tmp.txt
# # sed -i '/^$/d'  ./raw/text/train2/words_tmp.txt # Deleting blank lines
# sort -u -o ./raw/text/train2/words_tmp.txt ./raw/text/train2/words_tmp.txt


# ##########Sentances in Test2###############
# awk '{print $2}' FS='\t' ./raw/text/test2/test_filt.txt > ./raw/text/test2/trans
# awk '{print $1}' FS='\t' ./raw/text/test2/test_filt.txt > ./raw/text/test2/utt
# echo "Creating LM model creation input file"
# while read line
# do
# echo "<s> $line </s>" >> ./raw/text/test1/lm_test.txt
# done <./raw/text/test1/trans



####Phonetic Lexicon Creator######

#Copy words_tmp.txt to the directory of unifiedparser , run the following command there and copy lexicon_tmp.txt to ./raw/text/train2/
#cat words_tmp.txt | while read x ; do ./unified-parser $x 0 0 0 0 ; cat wordpronunciation >>lexicon_tmp.txt ; done


#############Lexicon Cleanup################
##TO CLEANUP THE OUTPUT OF UNIFIED PARSER
# filename='/home/kavya/researchdev/ml_asr/raw/language/lexicon.txt'
# sed -i 's/\"//g' $filename
# sed -i 's/(//g' $filename
# sed -i 's/set\!//g' $filename
# sed -i 's/ wordstruct //g' $filename
# sed -i 's/ ) 0) ))//g' $filename
# sed -i 's/)/ /g' $filename
# sed -i 's/0//g' $filename
# sed -i 's/    /9/g' $filename
# sed -i 's/9  /9/g' $filename
# sed -i 's/  /9/g' $filename
# sed -i 's/\x27/8/g' $filename
# sed -i 's/89//g' $filename
# sed -i 's/9/ /g' $filename

# # Make the new phonetic lexicon (phonetic dictionary) 
# paste ./raw/text/iiith/words.txt ./raw/text/iiith/uplex_iiith.txt >> ./raw/text/iiith/uplexicon-iiith.txt
# echo '!sil	SIL' >>  ./raw/language/uplexicon-all.txt
# echo '<unk>	SPN' >> ./raw/language/uplexicon-all.txt
# sort -u -o ./raw/language/uplexicon-all.txt ./raw/language/uplexicon-all.txt



#################Lexicon Clean up script for train1 and test1##########
# cp ./raw/language/lexicon_new ./raw/language/lexicon_tmp.txt
# sed -i 's/		 /	/g' ./raw/language/lexicon_tmp.txt # Replace '		 ' with '	'
# sed -i '/\./d' ./raw/language/lexicon_tmp.txt # Remove all lines containing '.'
# sed -i 's/  / /g' ./raw/language/lexicon_tmp.txt # Replace double spaces with single space
# sed -i '/,/d' ./raw/language/lexicon_tmp.txt # Remove all lines containing ','
# awk '{ sub(/[ \t]+$/, ""); print }' ./raw/language/lexicon_tmp.txt > ./raw/language/lexicon.txt
# rm ./raw/language/lexicon_tmp.txt
#####################

# #################Lexicon Clean up script-2##########
# cp ./raw/language/mal_dict ./raw/language/lexicon_tmp.txt
# sed -i 's/		 /	/g' ./raw/language/lexicon_tmp.txt # Replace '		 ' with '	'
# sed -i '/\./d' ./raw/language/lexicon_tmp.txt # Remove all lines containing '.'
# sed -i 's/  / /g' ./raw/language/lexicon_tmp.txt # Replace double spaces with single space
# sed -i '/,/d' ./raw/language/lexicon_tmp.txt # Remove all lines containing ','
# awk '{ sub(/[ \t]+$/, ""); print }' ./raw/language/lexicon_tmp.txt > ./raw/language/mal_lexicon.txt
# rm ./raw/language/lexicon_tmp.txt
#####################

#################################Utterance ID extraction#############
# filename='/home/kavya/researchdev/data_iitm/IITM_Fulldata/mono_male/txt.done.data'
# cp $filename /home/kavya/researchdev/data_iitm/IITM_Fulldata/mono_male/transcripts
# sed -i 's/(//g' /home/kavya/researchdev/data_iitm/IITM_Fulldata/mono_male/transcripts
# sed -i 's/ ")//g' /home/kavya/researchdev/data_iitm/IITM_Fulldata/mono_male/transcripts
# sed -i 's/ "/	/g' /home/kavya/researchdev/data_iitm/IITM_Fulldata/mono_male/transcripts
# awk '{print $1}' FS='\t' ./data/train/text > /data/train/textutt
# awk '{print $2}' FS='\t' ./data/train/text > /data/train/trans
#################################

#############################Clean up Transcripts############
# filename='./raw/text/train/transcriptold.tsv'
# sed -i 's/(//g' $filename
# sed -i 's/ ")//g' $filename
# sed -i 's/ "/	/g' $filename


# filename='./raw/text/iiith/transcript.tsv'
# sed -i 's/( //g' $filename
# sed -i 's/ " /	/g' $filename
# # sed -i 's/ ")//g' $filename
# awk '{print $1}' FS='\t' ./raw/text/iiith/transcript.tsv > ./raw/text/iiith/textutt
# awk '{print $2}' FS='\t'  ./raw/text/iiith/transcript.tsv > ./raw/text/iiith/trans

# # Find all unique words in transcript
# filename='./raw/text/iiith/trans'
# tr ' ' '\n' < $filename | sort | uniq > ./raw/text/iiith/words.txt # Find all unique words and write it in .words.txt


##Merge and Sort Lexicon Files#####
#sort ./raw/language/lexicon_new ./raw/language/mal_lexicon.txt | uniq > ./raw/language/lexicon

#### Total Duration of all audio files in a directory###
#soxi -D * | awk '{SUM += $1} END { printf "%d:%d:%d\n",SUM/3600,SUM%3600/60,SUM%60}'


### To serach for lines in dup in another text file lexicon.txt and write to out.txt
#grep -Ff dup out.txt > outnew.txt

###Find Unique words in Vasanavikruthi
# tr ' ' '\n' < ./raw/text/vasanavikruthi/vasanavikruthi.txt | sort | uniq > ./raw/text/vasanavikruthi/words.txt # Find all unique words and write it in .word_tmp.txt

# for folder in iiith openslr vasanavikruthi indictts
# do
# cat ./raw/wavdata/$folder/words.txt >> ./raw/language/words.txt
# done

# sort -u  ./raw/language/words.txt > ./raw/language/words-all.txt

# echo "Creating LM model creation input file"
# for folder in iiith openslr vasanavikruthi indictts
# do 
# while read line
# do
# echo "<s> $line </s>" >> ./raw/language/lm_train.txt
# done <./raw/wavdata/$folder/trans
# done
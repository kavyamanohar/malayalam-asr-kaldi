async function submitHandler(event) {
  document.getElementById("progress").style.display = "block";
  document.getElementById("result").innerText = '';

  const fileField = document.querySelector('input[type="file"]');
  const result = await doRecognize( fileField.files[0],fileField.files[0]['name'] )
  document.getElementById("progress").style.display = "none";
  document.getElementById("result").innerText = result.text;
}

function doRecognize(audio, filename="audio.webm"){
  document.getElementById("progress").style.display = "block";

  const formData = new FormData();
  formData.append("audio", audio, filename);
  return fetch("/api/recognize", {
    method: "POST",
    enctype: "multipart/form-data",
    body: formData,
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error("Error:", error);
    });
}

window.onload = (event) => {
  let rec,blob
  const startRecord = document.getElementById("startRecord");
  const stopRecord = document.getElementById("stopRecord");
  const recordedAudio=document.getElementById('recordedAudio');
  const blobSubmit=document.getElementById('blobSubmit');
  const audioDownload=document.getElementById('audioDownload');


  blobSubmit.onclick = async (e) => {
    document.getElementById("result").innerText = '';
    const result = await doRecognize( blob,  URL.createObjectURL(blob)+'.webm')
    document.getElementById("progress").style.display = "none";
    document.getElementById("result").innerText = result.text;
  }

  startRecord.onclick = (e) => {
    startRecord.disabled = true;
    stopRecord.disabled = false;
    // This will prompt for permission if not allowed earlier
    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then((stream) => {
        var audioChunks = [];
        rec = new MediaRecorder(stream);
        rec.ondataavailable = (e) => {
          audioChunks.push(e.data);
          if (rec.state == "inactive") {
            blob = new Blob(audioChunks, { type: "audio/webm" });
            recordedAudio.src = URL.createObjectURL(blob);
            audioDownload.href = recordedAudio.src;
            audioDownload.download = "audio.webm";
          }
        };
        rec.start();
      })
      .catch((e) => console.log(e));
  };

  stopRecord.onclick = (e) => {
    startRecord.disabled = false;
    stopRecord.disabled = true;
    rec.stop();
  };
};

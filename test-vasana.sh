#set-up for single machine or cluster based execution
. ./cmd.sh
#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh
basepath='.'

# for folder in vasanavikruthi 
# do
# mkdir data/$folder

# echo ============================================================================
# echo "                  Preparing Data Files for Featue extraction  for $folder	        "
# echo ============================================================================



# #Read path of wave files and store it as a temporary file wavefilepaths.txt 
# realpath ./raw/waves/$folder/*.wav > ./data/$folder/wavefilepaths.txt

# echo "Creating the list of utterence IDs"

# #The function is to extract the utterance id
# cat ./data/$folder/wavefilepaths.txt | xargs -l basename -s .wav > ./data/$folder/utt


# echo "Creating the list of utterence IDs mapped to absolute file paths of wavefiles"


# #Create wav.scp mapping from uttrence id to absolute wave file paths
# paste ./data/$folder/utt ./data/$folder/wavefilepaths.txt > ./data/$folder/wav.scp

# rm ./data/$folder/wavefilepaths.txt

# #Create utt2spk
# cat ./raw/wavdata/$folder/utt2spk > ./data/$folder/utt2spk

# echo "Creating the list of Speaker IDs mapped to corresponding list of utterance Ids"


# echo ============================================================================
# echo "                   Fixing Data directories               	        "
# echo ============================================================================

# ./utils/fix_data_dir.sh $basepath/data/$folder

# #Create spk2utt
# ./utils/utt2spk_to_spk2utt.pl ./data/$folder/utt2spk > ./data/$folder/spk2utt


# echo ============================================================================
# echo "     MFCC Feature Extraction and Mean-Variance Tuning Files for $folder    	        "
# echo ============================================================================


# #Create feature vectors
# ./steps/make_mfcc.sh --nj 6 data/$folder exp/make_mfcc/$folder mfcc

# #Copy the feature in text file formats for human reading
# copy-feats ark:./mfcc/raw_mfcc_$folder.1.ark ark,t:./mfcc/raw_mfcc_$folder.1.txt


# #Create Mean Variance Tuning
# steps/compute_cmvn_stats.sh data/$folder exp/make_mfcc/$folder mfcc


# echo "Creating the text file of uttid mapped to transcript separated by tab in sorted order"
# sort ./raw/wavdata/$folder/transcript.tsv > ./data/$folder/text


# echo "Creating the list of utterance"
# # First part of text delimited by tab
# awk '{print $1}' FS='\t' ./data/$folder/text > ./data/$folder/textutt

# echo "Creating the list of transcripts"
# # Second part of text delimited by tab
# awk '{print $2}' FS='\t' ./data/$folder/text > ./data/$folder/trans



# echo ============================================================================
# echo "                   Fixing Data directories               	        "
# echo ============================================================================

# ./utils/fix_data_dir.sh $basepath/data/$folder


# done

# train_dict=dict_up
# train_lang=lang_model_up
# exp=exp_up

train_dict=dict_mlphon
train_lang=lang_model_mlphon
exp=exp_mlphon

test_folder=vasanavikruthi
nj=1

./utils/fix_data_dir.sh $basepath/data/$test_folder

# echo
# echo "===== MONO DECODING ====="
# echo
# steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" $exp/mono/graph data/$test_folder $exp/mono/decode

# echo "Saving Results"
# cat $exp/mono/decode/scoring_kaldi/best_wer >> $exp/RESULTS/$test_folder

# echo "===== TRI1 (first triphone pass) DECODING ====="
# echo
# for sen in 200 ; do 
# for gauss in 16000; do 

# echo "========================="
# echo " Sen = $sen  Gauss = $gauss"
# echo "========================="

# steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" $exp/tri_$sen\_$gauss/graph data/$test_folder $exp/tri_$sen\_$gauss/decode

# echo "Saving Results"
# cat $exp/tri_$sen\_$gauss/decode/scoring_kaldi/best_wer >> $exp/RESULTS/$test_folder


# done;done


# echo "===== TRI_LDA (second triphone pass) DECODING ====="
# echo

# for sen in 400; do 
# for gauss in 17000 ; do 

# echo "========================="
# echo " Sen = $sen  Gauss = $gauss"
# echo "========================="

# steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" $exp/tri_$sen\_$gauss\_lda/graph data/$test_folder $exp/tri_$sen\_$gauss\_lda/decode
# cat $exp/tri_$sen\_$gauss\_lda/decode/scoring_kaldi/best_wer >> $exp/RESULTS/$test_folder

# done; done

# echo "===== TRI_SAT (third triphone pass) DECODING ====="
# echo

# for sen in 550 ; do 
# for gauss in 18000; do 
# echo "========================="
# echo " Sen = $sen  Gauss = $gauss"
# echo "========================="

# steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" $exp/tri_$sen\_$gauss\_sat/graph data/$test_folder $exp/tri_$sen\_$gauss\_sat/decode
# cat $exp/tri_$sen\_$gauss\_sat/decode/scoring_kaldi/best_wer >> $exp/RESULTS/$test_folder
#  done; done


echo "===== DNN Hybrid Testing DECODING ====="
echo

for hiddenlayersize in 3 ; do 
for minibatchsize in 128; do 
for nodes in 256; do
for numepochs in 10 ; do
echo "========================="
echo " hiddenlayer = $hiddenlayersize  minibatchsize = $minibatchsize nodes = $nodes numepochs=$numepochs "
# echo "========================="


steps/nnet2/decode.sh --cmd "$decode_cmd" --nj "$nj"  \
 $exp/tri_400_17000_lda/graph data/$test_folder \
  $exp/DNN_tri_lda_aligned_highlr_$hiddenlayersize\_$nodes\_$numepochs/decode

cat $exp/DNN_tri_lda_aligned_highlr_$hiddenlayersize\_$nodes\_$numepochs/decode/scoring_kaldi/best_wer >> $exp/RESULTS/$test_folder

done; done; done done
# echo "===== TDNN Hybrid Testing DECODING ====="
# echo

# steps/nnet3/decode.sh --cmd "$decode_cmd" --nj "$nj" \
#  exp/tri_sat/graph data/$test_folder \
#   exp/TDNN_tri_sat_aligned/decode


# cat exp/tri1/decode/scoring_kaldi/best_wer >> RESULT/new_test.txt
# cat exp/tri_lda/decode/scoring_kaldi/best_wer >> RESULT/new_test.txt
# cat exp/tri_sat/decode/scoring_kaldi/best_wer >> RESULT/new_test.txt
# cat exp/DNN_tri_sat_aligned_layer3_nodes256/decode/scoring_kaldi/best_wer >> RESULT/new_test.txt
# cat exp/TDNN_tri_sat_aligned/decode/scoring_kaldi/best_wer >> RESULT/new_test.txt

echo ============================================================================
echo "                   End of Script             	        "
echo ============================================================================

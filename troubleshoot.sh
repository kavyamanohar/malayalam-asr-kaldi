#!/bin/bash
# http://jrmeyer.github.io/asr/2016/02/01/Kaldi-notes.html
echo "=========Monophone GMM Info========="
/home/kavya/kavyadev/kaldi/src/gmmbin/gmm-info ./exp/mono/final.mdl 

echo "=========Monophone Tree Info========="
/home/kavya/kavyadev/kaldi/src/bin/tree-info exp/mono/tree

echo "=========Triphone GMM Info========="
/home/kavya/kavyadev/kaldi/src/gmmbin/gmm-info ./exp/tri1/final.mdl 

echo "=========Triphone Tree Info========="
/home/kavya/kavyadev/kaldi/src/bin/tree-info exp/tri1/tree

echo "===Monophone Context Dependent tree==="
/home/kavya/kavyadev/kaldi/src/bin/draw-tree ./data/lang_bigram/phones.txt ./exp/mono/tree | dot -Gsize=8,10.5 -Tps | ps2pdf - ./troubleshoot/mono-tree.pdf

# convert alignments to ctm
echo "===convert alignments to ctm in mono-ali==="
./steps/get_train_ctm.sh ./data/train2/ ./data/lang_bigram ./exp/mono_ali/


# print alignments to human readable format
/home/kavya/kavyadev/kaldi/src/bin/show-alignments ./data/lang_bigram/phones.txt ./exp/mono/final.mdl ark:"gunzip -c ./exp/mono/ali.2.gz |" > ./troubleshoot/ali.2.txt

# pull out one utt from ctm file
grep mlf_03182_00008388243 ./exp/mono_ali/ctm 

# foo here is a file that contains one uttID
echo "mlf_03182_00008388243"> ./troubleshoot/foo.txt

# compile that one graph
/home/kavya/kavyadev/kaldi/src/bin/compile-train-graphs ./exp/mono/tree ./exp/mono/0.mdl ./data/lang_bigram/L.fst 'ark:utils/sym2int.pl --map-oov 267 -f 2- ./data/lang_bigram/words.txt < ./troubleshoot/foo.txt |' 'ark:./exp/mono/test.fst' 

# get rid of its utt id at the beginning of file and print
echo "Print the Training graph for one utterance"
sed "s/mlf_03182_00008388243 //g" ./exp/mono/test.fst | /home/kavya/kavyadev/kaldi/tools/openfst-1.6.7/bin/fstprint --osymbols=./data/lang_bigram/words.txt --isymbols=./data/lang_bigram/phones.txt


# Phone level alignments ( CTM style )
/home/kavya/kavyadev/kaldi/src/bin/ali-to-phones --ctm-output ./exp/mono_ali/final.mdl ark:"gunzip -c ./exp/mono_ali/ali.2.gz |"  -> ali.2.ctm ; grep "mlf_03182_00008388243" ali.2.ctm

/home/kavya/kavyadev/kaldi/src/bin/show-alignments ./data/lang_bigram/phones.txt ./exp/mono/final.mdl ark:"gunzip -c ./exp/mono/ali.2.gz |" | grep "mlf_03182_00008388243"


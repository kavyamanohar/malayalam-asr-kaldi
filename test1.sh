#set-up for single machine or cluster based execution
. ./cmd.sh
#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh
basepath='.'

for folder in test1 
do
mkdir data/$folder

echo ============================================================================
echo "                  Preparing Data Files for Featue extraction  for $folder	        "
echo ============================================================================



#Read path of wave files and store it as a temporary file wavefilepaths.txt 
realpath ./raw/waves/$folder/*.wav > ./data/$folder/wavefilepaths.txt

echo "Creating the list of utterence IDs"

#The function is to extract the utterance id
cat ./data/$folder/wavefilepaths.txt | xargs -l basename -s .wav > ./data/$folder/utt


echo "Creating the list of utterence IDs mapped to absolute file paths of wavefiles"


#Create wav.scp mapping from uttrence id to absolute wave file paths
paste ./data/$folder/utt ./data/$folder/wavefilepaths.txt > ./data/$folder/wav.scp


echo "Creating the list of speaker IDs"

#Create speaker id list
if [ $folder == "test1" ]
then
cat ./data/$folder/utt | cut -d 's' -f 1 > ./data/$folder/spk
elif [ $folder == "test2" ]
then
cat ./data/$folder/utt | cut -d '_' -f1-2 > ./data/$folder/spk
# else
# cat ./data/$folder/utt | cut -d '_' -f1-2 > ./data/$folder/spk
fi

echo "Creating the list of Utterance IDs mapped to corresponding speaker Ids"

#Create utt2spk
paste ./data/$folder/utt ./data/$folder/spk > ./data/$folder/utt2spk

echo "Creating the list of Speaker IDs mapped to corresponding list of utterance Ids"

#Create spk2utt
./utils/utt2spk_to_spk2utt.pl ./data/$folder/utt2spk > ./data/$folder/spk2utt

rm ./data/$folder/wavefilepaths.txt


echo ============================================================================
echo "                   Fixing and Test Data directories               	        "
echo ============================================================================

./utils/fix_data_dir.sh ./data/$folder

echo ============================================================================
echo "     MFCC Feature Extraction and Mean-Variance Tuning Files for $folder    	        "
echo ============================================================================


#Create feature vectors
./steps/make_mfcc.sh --nj 4 data/$folder exp/make_mfcc/$folder mfcc

#Copy the feature in text file formats for human reading
copy-feats ark:./mfcc/raw_mfcc_$folder.1.ark ark,t:./mfcc/raw_mfcc_$folder.1.txt


#Create Mean Variance Tuning
steps/compute_cmvn_stats.sh data/$folder exp/make_mfcc/$folder mfcc


echo "Creating the text file of uttid mapped to transcript separated by tab in sorted order"
sort ./raw/text/$folder/transcript.tsv > ./data/$folder/text


echo "Creating the list of utterance"
# First part of text delimited by tab
awk '{print $1}' FS='\t' ./data/$folder/text > ./data/$folder/textutt

echo "Creating the list of transcripts"
# Second part of text delimited by tab
awk '{print $2}' FS='\t' ./data/$folder/text > ./data/$folder/trans



echo ============================================================================
echo "                   Fixing Data directories               	        "
echo ============================================================================

./utils/fix_data_dir.sh $basepath/data/$folder


done


test_folder=test1
nj=2


echo
echo "===== MONO DECODING ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/mono/graph data/$test_folder exp/mono/decode



echo "===== TRI1 (first triphone pass) DECODING ====="
echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri1/graph data/$test_folder exp/tri1/decode

echo "===== TRI_LDA (second triphone pass) DECODING ====="
# echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri_lda/graph data/$test_folder exp/tri_lda/decode


# echo "===== TRI_SAT (third triphone pass) DECODING ====="
# echo
steps/decode.sh --config conf/decode.config --nj $nj --cmd "$decode_cmd" exp/tri_sat/graph data/$test_folder exp/tri_sat/decode


echo "===== DNN Hybrid Testing DECODING ====="
echo

steps/nnet2/decode.sh --cmd "$decode_cmd" --nj "$nj" \
 exp/tri_sat/graph data/$test_folder \
  exp/DNN_tri_sat_aligned_layer3_nodes256/decode


echo "Saving Results"
cat exp/mono/decode/scoring_kaldi/best_wer >> RESULT/indictts.txt
cat exp/tri1/decode/scoring_kaldi/best_wer >> RESULT/indictts.txt
cat exp/tri_lda/decode/scoring_kaldi/best_wer >> RESULT/indictts.txt
cat exp/tri_sat/decode/scoring_kaldi/best_wer >> RESULT/indictts.txt
cat exp/DNN_tri_sat_aligned_layer3_nodes256/decode/scoring_kaldi/best_wer >> RESULT/indictts.txt


echo ============================================================================
echo "                   End of Script             	        "
echo ============================================================================
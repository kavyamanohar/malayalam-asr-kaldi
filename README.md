This is a [kaldi](https://kaldi-asr.org/) based recipie for Malayalam speech recognition. You need a working Kaldi directory to run this script.

Details on how to run this script and the working is described here.

To install Kaldi, see the documentation [here](https://kaldi-asr.org/doc/install.html)

The source code of `/ml_asr` has to be placed in the `/egs` directory of Kaldi installation directory.

# RAW DATA 


`/raw` has all the data available at the beginning of the project

- `/wav` (wav files. File names are of the form `utteranceID.wav`)
    - `/train1` (Indic TTS. 12 hr 39 minutes. 2 speakers( -  1F 1M)
    - `/test1` (Indic TTS. 1 hr 18 minutes. 2 speakers. Same speakers as in test1, but different utterances)
    - `/train2` (Open SLR. 4 hr 39 minutes. 34 speakers- 20F, 14 M)
    - `/test2` (Open SLR. 55 minutes. 8 speakers - 4F, 4M)
- `/text`(transcript of wav files by the name `transcript.tsv`)
    - These are placed in directories below. `train1` and `test1` shares the same transcripts. Similarly `train2` and `test2`.
        - `/train2`
        - `/test2`
        - `/train1`
        - `/test1`
        - `cleanup.sh` in the root directory contains script to clean up the transcript. It removes punctuations and non-Malayalam content. It should be adapted for specific transcript sources.
        `trans` file contains the sentences extracted from `transcript.tsv` 
        - Words extracted from the transcripts are stored in files named as words_tmp.txt.
        - Indic TTS contains a total 0f 21751 words, Open SLR contains a total of 7315 words. 
    - Removing the duplicates there are 26647 words common to both and is stored as a `words.txt` in `/text` directory.
    - Phonetic transcription of these 26647 are available in `lexicon.txt`. It was created using mlphon library.
- `/language`
    - `/lexicon` (the phonetic transcript of words in the language vocabulary)
    -  There are 26592 words in the lexicon dictionary. Certain words with wrong syllable structure that could not be converted into phonemes were discarded.
    ### TODO: Correct those spellings in the transcript itself



# SPEECH FEATURE EXTRACTION

To extract the features from audio clips, run the following script. 
```
$./extractfeatures.sh
```
It prepares the data and extract features as described below. First of all it removes old `/exp`, `/data` and `/mfcc` directories.

## DATA PREPARATION

From the `/raw` directory a `/data` directory is created with the following contents. This representation is important for further processing with kaldi tools.

- `/data`
    - `/train`
        - `utt` (List of utterance IDs)
        - `wav.scp` (Utterance IDs mapped to absolute wav file paths)
        - `spk` (List of speaker IDs)
        - `utt2spk` (List of utterences corresponding to a speaker)
        - `spk2utt` (Speaker mapped to every utterance ID)
<!-- 
    - `/test`
        - `utt` (List of utterance IDs)
        - `wav.scp` (Utterance IDs mapped to absolute wav file paths)
        - `spk` (List of speaker IDs)
        - `utt2spk` (List of utterences corresponding to a speaker)
        - `spk2utt` (Speaker mapped to every utterance ID) -->


## MFCC and CMVN

MFCC features are extracted and stored in `.ark` format in `/mfcc` directory. CMVN tuned features are also in the same directory in `.ark` format. The absolute filepaths of these ark files corresponding to each speaker (for cmvn) and for each utterance (for raw mfcc) are stored in corresponding `.scp` files. This needs the data prepared in the previous step.

- `/mfcc`
    - `raw_mfcc_train.ark`
    - `raw_mfcc_test.ark`
    - `cmvn_train.ark`
    - `cmvn_test.ark`
    - `raw_mfcc_test.scp`
    - `raw_mfcc_train.scp`
    - `cmvn_train.scp`
    - `cmvn_test.scp`

In parallel `/data/train` and `/data/test` are also populated with `cmvn.scp` and  `feats.scp`

 The log files for these feature extraction process are stored in `/exp/make_mfcc` in separate sub directories

 - `/exp/make_mfcc`
    - `/test`
        - log files
    - `/train`
        - log files


# LANGUAGE MODEL CREATION

It creates the lexicon dictionary and the language model grammar. For that, run the following script. 
```
$./createLM.sh
```
Note that it uses the data folders previously prepared by the `$./extractfeatures` script. So make sure you run that script prior to `$./createLM.sh`. It prepares the lexicon dictionary and n-gram language model as described below.

## DATA PREPARATION

It runs on the training data directory. From the `/raw` data directory of transcrips create files of utterenceID, speech trascript, and their mapping files.

- `/data`
    - `/train`
        - `textutt` (List of utteranceIDs. It is currenty same as utt)
        - `trans` (List of all transcripts)
        - `text` (UtteranceID to transcript mapping. Same as `transcript.tsv` in the `/raw` data directory)
        - `lm_train.txt` (Lit of utterances with sentence begin and end markers. This is the file used for n-gram LM creation)

From the `/raw` data directory of language vocabulary lexicon, a list of phones in `/data/local/dict`

- `/data`
    - `/local`
        - `/dict`
            - extra_phones.txt
            - extra_questions.txt
            - lexiconp .txt  
            - lexicon.txt
            - nonsilence_phones.txt
            - optional_silence.txt
            - phones.txt
            - silence_phones.txt

## Creation of Lexical dictionary 

`utils/prepare_lang.sh` creates the lexical dictionary as an FST in `data/lang_ngram/L.fst`

## N-gram language model creation

Once the data is ready n-gram language model can be created. Here it is done using IRSTLM toolkit. It produces language model in ARPA format. Final language model in FST format, `G.fst` is available in `/data/lang_ngram/G.fst`.

## Fixing the train and test directories

It removes the list of transcripts if the acoustic features were not extracted for some utterance ids. This is required because we have the same set of transcripts in both `train1` and `test1` directories. Removing the unwanted transcripts makes scoring easy after decoding.

# TRAINING GMM-HMM or DNN

To run the script for training and decoding,

```
$train.sh
```

There are different options for training and Decoding. 

- monophone (uses mfcc, delta and delta delta featutes)
- triphone (uses mfcc, delta and delta delta featutes)
- triphone LDA (uses mfcc features for deriving splice features and later transform to lda)
- triphone SAT //Currently this code is not in use.
- DNN (tanh training after obtaining alignment info from tri_lda)

Each subsequent model is trained after obtaining alignment information from the previous model.

Training is followed by creation of graphs. These graphs are later looked up for decoding.


# TESTING the model

- Once training is done, there will be decoding graphs available in `/exp` directory.
- Decoding involves creating  word lattices for the utterances under consideration looking up the graphs
- From the lattice, one best path decoding is done
- The scoring function computes word error rate (WER) and Sentance error rates (SER) in percentage

To decode the Open SLR test set, run
```
$test2.sh
```

To decode the Indic TTS test set, run
```
$test1.sh
```

It prepares the utt, spk mappings, extracts features before running the decoding.

<!-- # DO FEATURE EXTRACTION, LM CREATION, TRAINING and DECODING all at once 

**This script is now outdated**

```
$run.sh
``` -->


# TRANSCRIBE your Speech

## If you have a set of audio to be transcribed

If you have an audio clip of Malayalam digit utterance, you can transcribe it using the trained DNN model in `/exp`. Keep your audio file in wav format in `/inputaudio` directory and run,

```
$./speech2text.sh
```

The result will in `./inputaudio/transcriptions/one-best-hypothesis.txt`

## If you have a single audio to be transcribed

```
$./quick_asr.sh audio.wav
$./quick_asr.sh audio.webm
```
It functions the same way as `./speech2text.sh`

## Web interface to decode speech

- run the python server 

```
$ python server.py
```

- Goto the link `https://0.0.0.0:8080/` from browser
- Upload a webm/wav file or record using the interface and press submit button
- Decoded output is displayed on the interface.
- It internally invokes `./quick_asr.sh` 

# Docker Image of Recognizer (Uses old DNN model for decoding)

To build a docker image of the recognizer with DNN decoder invoked by `quick-asr.sh`, run the following command,

```
docker build --tag kavyamanohar/ml_asr:latest .
```

Alternately you can pull the image from dockerhub,

```
docker pull kavyamanohar/ml_asr:latest
```

To run the image with port 8080 of the container linked to port 4040 of the local-host

```
docker run -it -p 4040:8080 kavya/asr:ml
```

You will be in the container's interactive terminal at the location given below:

```
root@6234fc4a9a32:/opt/kaldi/egs/ml_asr#
```
You can run the decoding using `./quick-asr.sh` and the `example.wav` file present in the docker image

```
root@6234fc4a9a32:/opt/kaldi/egs/ml_asr# ./quick-asr.sh example.wav
```

You can run the server using 

```
root@6234fc4a9a32:/opt/kaldi/egs/ml_asr# python3 server.py
```

- Goto the link `localhost:4040` from browser
- Upload a webm/wav file or record using the interface and press submit button
- Decoded output is displayed on the interface.


!sil	SIL
<unk>	SPN
അകത്തുകടന്ന്	a k a t t u k a tx a n n
അക്കാലത്ത്	a k k aa l a t t
അച്ഛനാണ്	a c ch a n aa nx
അച്ഛനെയല്ല	a c ch a n e y a l l a
അച്ഛൻ	a c ch a nn
അഞ്ചു	a nj c u
അഞ്ചുറുപ്പിക	a nj c u rx u p p i k a
അഞ്ചെട്ടു	a nj c e tx tx u
അടുത്തകാലത്തിന്നുള്ളിൽ	a tx u t t a k aa l a t t i n n u lx lx i lw
അടുത്തുചെന്നു	a tx u t t u c e n n u
അതാണ്	a t aa nx
അതികേമമായിരുന്നു	a t i k ee m a m aa y i r u n n u
അതിനിടയിൽ	a t i n i tx a y i lw
അതിപ്രേമമായിരുന്നു	a t i p r ee m a m aa y i r u n n u
അതിൽ	a t i lw
അതിസൗഭാഗ്യവതിയായ	a t i s au bh aa g y a w a t i y aa y a
അതു	a t u
അതും	a t u q
അതുകൊണ്ട്	a t u k o nx tx
അതുണ്ടായില്ല	a t u nx tx aa y i l l a
അതുപോലെ	a t u p oo l e
അത്	a t
അത്ര	a t r a
അദ്ദേഹം	a d d ee h a q
അദ്ദേഹത്തിന്റെ	a d d ee h a q
അധികം	a dh i k a q
അനന്തസേവാ	a n a n t a s ee w aa
അനുഭവം	a n u bh a w a q
അനുഭവിക്കുന്നതിൽ	a n u bh a w i k k u n n a t i lw
അനുഭവിക്കുന്നവരും	a n u bh a w i k k u n n a w a r u q
അനുഭവിച്ചവരും	a n u bh a w i c c a w a r u q
അനുഭവിച്ചിട്ടുള്ളതിൽ	a n u bh a w i c c i tx tx u lx lx a t i lw
അനുരാഗമുണ്ടായിരുന്നു	a n u r aa g a m u nx tx aa y i r u n n u
അന്നു	a n n u
അന്വേഷണം	a n w ee sx a nx a q
അപമാനമില്ല	a p a m aa n a m i l l a
അപമാനമേയുള്ളൂ	a p a m aa n a m ee y u lx lx uu
അപ്പോൾ	a p p oo ln
അപ്പോഴേക്ക്	a p p oo zh ee k k
അഭിപ്രായക്കാരനായിരുന്നില്ല	a bh i p r aa y a k k aa r a n aa y i r u n n i l l a
അഭ്യസിച്ചിരിക്കണം	a bh y a s i c c i r i k k a nx a q
അമര്യാദക്കാരുമായിട്ടാണ്	a m a r y aa d a k k aa r u m aa y i tx tx aa nx
അമര്യാദതാവഴിയിലാണ്	a m a r y aa d a t aa w a zh i y i l aa nx
അമര്യാദതാവഴിയിൽ	a m a r y aa d a t aa w a zh i y i lw
അമാനുഷൻ	a m a r y aa d a t aa w a zh i y i lw
അമ്മാവനും	a m m aa w a n u q
അയാളെ	a y aa lx e
അർജുനന്റെ	a rw j u n a n rx e
അർഥം	a rw th a q
അല്ലേ	a l l ee
അവനായിട്ട്	a w a n aa y i tx tx
അവന്റെ	a w a n rx e
അവമാനം	a w a m aa n a q
അവരിൽ	a w a r i lw
അവരുടെ	a w a r u tx e
അവശേഷിക്കുന്ന	a w a sh ee sx i k k u n n a
അവളേയും	a w a lx ee y u q
അവൾക്കു	a w a ln k k u
അവിടെച്ചെന്നാൽ	a w i tx e c c e n n aa lw
അശേഷം	a sh ee sx a q
അസ്വാധീനത്തിങ്കലും	a s w aa dh ii n a t t i ng k a l u q
അറിയാതെ	a rx i y aa t e
അറിയാവുന്നതായിരുന്നാൽ	a rx i y aa w u n n a t aa y i r u n n aa lw
അറിവാൻ	a rx i w aa nn
അറിവ്	a rx i w
ആ	aa
ആണ്	aa nx
ആദ്യം	aa d y a q
ആപത്തിനുള്ള	aa p a t t i n u lx lx a
ആപത്തുകളെ	aa p a t t u k a lx e
ആഭരണപ്പെട്ടി	aa bh a r a nx a p p e tx tx i
ആയി	aa y i
ആലോചിച്ചു	aa l oo c i c c u
ആറുമാസവും	aa rx u m aa s a w u q
ഇക്കണ്ടകുറുപ്പിന്റെ	i k k a nx tx a k u rx u p p i n rx e
ഇക്കണ്ടക്കുറുപ്പ്	i k k a nx tx a k k u rx u p p
ഇങ്ങനെ	i ng ng a n e
ഇടയുണ്ട്	i tx a y u nx tx
ഇടയ്‌ക്കിടെ	i tx a y k k i tx e
ഇടുവിച്ചു	i tx u w i c c u
ഇട്ടിനാരായണൻ	i tx tx i n aa r aa y a nx a nn
ഇട്യാറാണന്റെ	i tx y aa rx aa nx a n rx e
ഇട്യാറാണാൻ	i tx y aa rx aa nx aa nn
ഇതാണ്	i t aa nx
ഇതിലൊന്നിലും	i t i l o n n i l u q
ഇതു	i t u
ഇതുപോലെയാണ്	i t u p oo l e y aa nx
ഇതുവരെ	i t u w a r e
ഇത്ര	i t r a
ഇത്രവളരെക്കാലം	i t r a w a lx a r e k k aa l a q
ഇദ്ദേഹത്തിനു	i d d ee h a t t i n u
ഇദ്ദേഹത്തിനെ	i d d ee h a t t i n e
ഇനി	i n i
ഇനിക്കവിടെ	i n i k k a w i tx e
ഇനിക്കു	i n i k k u
ഇനിക്ക്	i n i k k
ഇന്നലെയും	i n n a l e y u q
ഇന്നും	i n n u q
ഇരിങ്ങാലക്കുട	i r i ng ng aa l a k k u tx a
ഇരിപ്പാൻ	i r i p p aa nn
ഇരുന്നാൽ	i r u n n aa lw
ഇരുപതു	i r u p a t u
ഇല്ല	i l l a
ഇല്ലത്തെ	i l l a t t e
ഇല്ലാഞ്ഞാൽ	i l l aa nj nj aa lw
ഇല്ലാതെവശായി	i l l aa t e w a sh aa y i
ഇല്ലെന്നല്ല	i l l e n n a l l a
ഇവനു	i w a n u
ഇവനെ	i w a n e
ഇവിടെ	i w i tx e
ഈ	ii
ഈരാറു	ii r aa rx u
ഉച്ചതിരിഞ്ഞ	u c c a t i r i nj nj a
ഉടനെ	u tx a n e
ഉണരാതിരിപ്പാൻ	u tx a n e
ഉണരാത്ത	u nx a r aa t t a
ഉണരുന്നത്	u nx a r u n n a t
ഉണരുമോ	u nx a r u m oo
ഉണർന്നു	u nx a rw n n u
ഉണ്ടായി	u nx tx aa y i
ഉണ്ടായിട്ടില്ല	u nx tx aa y i tx tx i l l a
ഉണ്ടായിരുന്നില്ല	u nx tx aa y i r u n n i l l a
ഉണ്ടായില്ല	u nx tx aa y i l l a
ഉണ്ടെന്നു	u nx tx e n n u
ഉണ്ട്	u nx tx
ഉത്സാഹിച്ചു	u t s aa h i c c u
ഉദ്യോഗസ്ഥന്മാരാൽ	u d y oo g a s th a n m aa r aa lw
ഉദ്യോഗസ്ഥന്മാർക്ക്	u d y oo g a s th a n m aa rw k k
ഉള്ളതാണ്	u lx lx a t aa nx
ഉറക്കമാണ്	u rx a k k a m aa nx
ഉറങ്ങിയിരുന്നത്	u rx a ng ng i y i r u n n a t
ഊരാഞ്ചാടിയായിരുന്നാലും	uu r aa nj c aa tx i y aa y i r u n n aa l u q
ഊരാറില്ല	uu r aa rx i l l a
എങ്കിലും	e ng k i l u q
എങ്ങനെയാണെന്നു	e ng ng a n e y aa nx e n n u
എങ്ങനെയാണ്	e ng ng a n e y aa nx
എടത്തെ	e tx a t t e
എടത്തെക്കൈയിന്റെ	e tx a t t e k k ai y i n rx e
എടുക്കണമെന്നുണ്ടായിരുന്നില്ല	e tx u k k a nx a m e n n u nx tx aa y i r u n n i l l a
എടുത്ത	e tx u t t a
എടുത്ത്	e tx u t t
എനിക്കിട്ടിട്ടുള്ളതും	e n i k k i tx tx i tx tx u lx lx a t u q
എനിക്കു	e n i k k u
എനിക്കുണ്ടായി	e n i k k u nx tx aa y i
എനിക്കുണ്ടായില്ല	e n i k k u nx tx aa y i l l a
എനിക്ക്	e n i k k
എന്ന	e n n a
എന്നാൽ	e n n aa lw
എന്നിങ്ങനെ	e n n i ng ng a n e
എന്നിട്ടാണ്	e n n i tx tx aa nx
എന്നു	e n n u
എന്നുമാത്രമല്ല	e n n u m aa t r a m a l l a
എന്നുലള്ളതിലേക്ക്	e n n u l a lx lx a t i l ee k k
എന്നെ	e n n e
എന്നെക്കാൾ	e n n e k k aa ln
എന്നെപ്പോലെ	e n n e p p oo l e
എന്നേയും	e n n ee y u q
എന്ന്	e n n
എന്റെ	e n rx e
എല്ലാ	e l l aa
എല്ലാം	e l l aa q
എല്ലാവരും	e l l aa w a r u q
എവിടെപ്പോയിരിക്കാമെന്ന്	e w i tx e p p oo y i r i k k aa m e n n
എഴുതുന്നില്ല	e zh u t u n n i l l a
എഴുന്നേറ്റ്	e zh u n n ee rx rx
ഏകസംബന്ധിജ്ഞാനമപരസംബന്ധിസ്‌മാരകമെന്ന	ee k a s a q b a n dh i j nj aa n a m a p a r a s a q b a n dh i s m aa r a k a m e n n a
ഏർപ്പെടുത്തി	ee rw p p e tx u t t i
ഒതുക്കാവുന്നതെല്ലാം	o t u k k aa w u n n a t e l l aa q
ഒന്നു	o n n u
ഒന്ന്	o n n
ഒപ്പ്	o p p
ഒരതിരും	o r a t i r u q
ഒരിക്കലും	o r i k k a l u q
ഒരില്ലത്താണ്	o r i l l a t t aa nx
ഒരു	o r u
ഒരുപോലെ	o r u p oo l e
ഒരുവകക്കാര്	o r u w a k a k k aa r
ഒരുവിധം	o r u w i dh a q
ഒഴിഞ്ഞുപോകണമെന്നു	o zh i nj nj u p oo k a nx a m e n n u
ഒറ്റയ്ക്കുപോയി	o rx rx a y k k u p oo y i
ഒറ്റയ്ക്കുള്ളതായിരിക്കുകയാണ്	o rx rx a y k k u lx lx a t aa y i r i k k u k a y aa nx
ഒറ്റ്	o rx rx
ഓർമവന്നപ്പോൾ	oo rw m a w a n n a p p oo ln
ഓർമവന്നു	oo rw m a w a n n u
ഓർമ്മയ്‌ക്കു	oo rw m m a y k k u
ഓഹരിയും	oo h a r i y u q
കക്കാറും	k a k k aa rx u q
കക്കുക	k a k k u k a
കടം	k a tx a q
കണ്ടപ്പോൾ	k a nx tx a p p oo ln
കണ്ടപ്പോൾത്തന്നെ	k a nx tx a p p oo ln t t a n n e
കണ്ടിട്ടുള്ള	k a nx tx i tx tx u lx lx a
കണ്ടില്ല	k a nx tx i l l a
കണ്ടുനിന്നു	k a nx tx u n i n n u
കണ്ടെത്താതിരിക്കയില്ല	k a nx tx e t t aa t i r i k k a y i l l a
കണ്ടെത്തിക്കിട്ടായാൽ	k a nx tx e t t i k k i tx tx aa y aa lw
കണ്ടെത്തിയാൽ	k a nx tx e t t i y aa lw
കണ്ട്	k a nx tx
കഥ	k a th a
കഥയില്ലായ്മ	k a th a y i l l aa y m a
കയ്‌പീത്തുകൊടുക്കാം	k a y p ii t t u k o tx u k k aa q
കയ്യിട്ടു	k a y y i tx tx u
കയ്യിലെടുത്തു	k a y y i l e tx u t t u
കയ്യിൽ	k a y y i lw
കരുതി	k a r u t i
കരുതിയാണ്	k a r u t i y aa nx
കല്യാണിക്കുട്ടിയെ	k a l y aa nx i k k u tx tx i y e
കല്ല്യാണിക്കുട്ടിക്കു	k a l l y aa nx i k k u tx tx i k k u
കഷ്‌ടമല്ല	k a sx tx a m a l l a
കളവിൽ	k a lx a w i lw
കളവു	k a lx a w u
കളവുകവിഞ്ഞതിൽ	k a lx a w u k a w i nj nj a t i lw
കളവുകളും	k a lx a w u k a lx u q
കളവുണ്ടായത്	k a lx a w u nx tx aa y a t
കളവുനടത്തി	k a lx a w u n a tx a t t i
കളവുപോയി	k a lx a w u p oo y i
കളവുവിട്ട്	k a lx a w u w i tx tx
കളവ്	k a lx a w
കള്ളനാവാനുള്ള	k a lx lx a n aa w aa n u lx lx a
കള്ളൻ	k a lx lx a nn
കഴിച്ച്	k a zh i c c
കഴിഞ്ഞപ്പോഴേക്കും	k a zh i nj nj a p p oo zh ee k k u q
കഴിഞ്ഞ്	k a zh i nj nj a p p oo zh ee k k u q
കഴിയില്ലെന്നു	k a zh i y i l l e n n u
കറപറ്റിയ	k a rx a p a rx rx i y a
കറുത്തും	k a rx a p a rx rx i y a
കറുപ്പുകൂടിയ	k a rx u p p u k uu tx i y a
കാടരികായിട്ടുള്ള	k aa tx a r i k aa y i tx tx u lx lx a
കാടരികിൽ	k aa tx a r i k i lw
കാട്ടിൽപോകുവാനും	k aa tx tx i lw p oo k u w aa n u q
കാരണവന്മാരുടെ	k aa r a nx a w a n m aa r u tx e
കാലത്തും	k aa r a nx a w a n m aa r u tx e
കാലത്തുതന്നെ	k aa l a t t u t a n n e
കാലത്തെ	k aa l a t t e
കാലത്തേ	k aa l a t t ee
കാവ്യം	k aa w y a q
കാഴ്‌ച	k aa zh c a
കിടന്നുറങ്ങുമ്പോൾ	k i tx a n n u rx a ng ng u m p oo ln
കിട്ടണമെന്നാണെന്നു	k i tx tx a nx a m e n n aa nx e n n u
കിട്ടിയെന്ന്	k i tx tx i y e n n
കിട്ടീട്ടുള്ള	k i tx tx ii tx tx u lx lx a
കിട്ടുന്ന	k i tx tx u n n a
കിട്ടുവാൻ	k i tx tx u w aa nn
കുടുങ്ങുന്ന	k u tx u ng ng u n n a
കുട്ടികൾക്കുകൂടി	k u tx tx i k a ln k k u k uu tx i
കുറച്ചു	k u rx a c c u
കുറച്ചുദിവസത്തേക്ക്	k u rx a c c u d i w a s a t t ee k k
കുറെ	k u rx e
കൂടാതെ	k uu tx aa t e
കൂടി	k uu tx i
കൂട്ടത്തിലേക്ക്	k uu tx tx a t t i l ee k k
കെട്ടി	k e tx tx i
കെട്ടിയിരുന്നത്	k e tx tx i y i r u n n a t
കെണിയാണെന്ന്	k e nx i y aa nx e n n
കേട്ടിരിക്കാതിരിക്കയില്ല	k ee tx tx i r i k k aa t i r i k k a y i l l a
കേൾക്കാത്ത	k ee ln k k aa t t a
കൈക്കലാക്കി	k ai k k a l aa k k i
കൈയിന്മേൽ	k ai y i n m ee lw
കൈയ്യിൽ	k ai y y i lw
കൈവശത്തിൽ	k ai w a sh a t t i lw
കൈവിലങ്ങും	k ai w i l a ng ng u q
കൊച്ചി	k o c c i
കൊച്ചിശ്ശീമയിലാണ്	k o c c i sh sh ii m a y i l aa nx
കൊടുക്കാവു	k o tx u k k aa w u
കൊടുങ്ങല്ലൂർ	k o tx u ng ng a l l uu rw
കൊടുത്ത	k o tx u t t a
കൊടുത്തിട്ടുണ്ടായിരുന്നു	k o tx u t t i tx tx u nx tx aa y i r u n n u
കൊടുത്തു	k o tx u t t u
കൊണ്ടുപിടിച്ചു	k o nx tx u p i tx i c c u
കൊല്ലത്തോളം	k o l l a t t oo lx a q
കൊള്ളരുതാത്ത	k o lx lx a r u t aa t t a
കൊള്ളാറും	k o lx lx aa rx u q
കോടതി	k oo tx a t i
കോടതിപൂട്ടൽപ്പോലെ	k oo tx a t i p uu tx tx a lw p p oo l e
കോണം	k oo nx a q
കോൺസ്റ്റബിൾ	k oo nw s rx rx a b i ln
കോന്ത്രമ്പല്ലും	k oo n t r a m p a l l u q
ഗംഗാസ്നാനവും	g a q g aa s n aa n a w u q
ഗണാഷ്‌ടകവ്യുൽപ്പത്തി	g a nx aa sx tx a k a w y u lw p p a t t i
ഗന്തർ	g a n t a rw
ഗംഭീരന്മാർ	g a q bh ii r a n m aa rw
ഗുജിലിത്തെരുവിൽ	g u j i l i t t e r u w i lw
ഗുരുനാഥൻ	g u r u n aa th a nn
ഗൃഹസ്ഥന്റെ	g rq h a s th a n rx e
ഗ്രാമക്കാർ	g r aa m a k k aa rw
ചാടിപ്പോന്നതിൽപ്പിന്നെ	c aa tx i p p oo n n a t i lw p p i n n e
ചിലരെങ്കിലും	c i l a r e ng k i l u q
ചിലർ	c i l a rw
ചില്ലറ	c i l l a rx a
ചീത്തയാണെന്നല്ലേ	c ii t t a y aa nx e n n a l l ee
ചുരുക്കമായിരിക്കും	c u r u k k a m aa y i r i k k u q
ചെന്ന	c e n n a
ചെന്നപ്പോൾ	c e n n a p p oo ln
ചെന്നുചാടുന്നത്	c e n n u c aa tx u n n a t
ചെന്ന്	c e n n
ചെയ്‌ത	c e y t a
ചെയ്‌തിട്ടുണ്ടായിരുന്നു	c e y t i tx tx u nx tx aa y i r u n n u
ചെയ്തു	c e y t u
ചെയ്‌ത്	c e y t
ചെയ്യട്ടെ	c e y y a tx tx e
ചെയ്യുന്നത്	c e y y u n n a t
ചെയ്വാൻ	c e y w aa nn
ചെല്ലുകയുള്ളൂ	c e l l u k a y u lx lx uu
ചൊല്ലാറുണ്ട്	c o l l aa rx u nx tx
ചോടു	c oo tx u
ചോദിക്കയും	c oo d i k k a y u q
ചോദിച്ചപ്പോൾ	c oo d i c c a p p oo ln
ജനത്തിരിക്കും	j a n a t t i r i k k u q
ജനനം	j a n a n a q
ജാതി	j aa t i
ഞങ്ങളുടെ	nj a ng ng a lx u tx e
ഞാനിതാ	nj aa n i t aa
ഞാനും	nj aa n u q
ഞാനെന്റെ	nj aa n e n rx e
ഞാനൊരു	nj aa n o r u
ഞാൻ	nj aa nn
ഞെട്ടി	nj e tx tx i
തടസ്സവും	t a tx a s s a w u q
തട്ടണമെന്ന്	t a tx tx a nx a m e n n
തന്നെ	t a n n e
തന്നെയാകുന്നു	t a n n e y aa k u n n u
തന്നെയാണ്	t a n n e y aa nx
തന്റെ	t a n rx e
തപ്പിനോക്കിയപ്പോൾ	t a p p i n oo k k i y a p p oo ln
തമ്മിലുള്ള	t a m m i l u lx lx a
തരമാകുന്നത്	t a r a m aa k u n n a t
തരമില്ലെന്നുതോന്നി	t a r a m i l l e n n u t oo n n i
തലമുറ	t a l a m u rx a
തലയ്ക്കൽ	t a l a y k k a lw
തലേ	t a l ee
തലേക്കെട്ടും	t a l ee k k e tx tx u q
തൽക്കാലം	t a lw k k aa l a q
തറവാട്ടിലും	t a rx a w aa tx tx i l u q
തറവാട്ടിൽ	t a rx a w aa tx tx i lw
താക്കീതു	t aa k k ii t u
താൻ	t aa nn
താമസവും	t aa nn
താമസിക്കുന്നേടത്തു	t aa m a s i k k u n n ee tx a t t u
താവഴിക്കാർ	t aa w a zh i k k aa rw
താവഴിയും	t aa w a zh i y u q
തിരുമനസ്സുകൊണ്ട്	t i r u m a n a s s u k o nx tx
തീർന്നു	t ii rw n n u
തു	t u
തുടങ്ങി	t u
തുടങ്ങിയതല്ല	t u tx a ng ng i y a t a l l a
തുമ്പും	t u m p u q
തുറന്ന്	t u rx a n n
തൃശ്ശിവപേരൂർക്ക്	t rq sh sh i w a p ee r uu rw k k
തെണ്ടി	t e nx tx i
തെണ്ടിനായാട്ടും	t e nx tx i n aa y aa tx tx u q
തെളിനായാട്ടായാൽ	t e lx i n aa y aa tx tx aa y aa lw
തെളിനായാട്ടും	t e lx i n aa y aa tx tx u q
തെറ്റിപ്പോകുവാൻ	t e rx rx i p p oo k u w aa nn
തേവിടിശ്ശി	t ee w i tx i sh sh i
തേവിടിശ്ശിയുടെ	t ee w i tx i sh sh i y u tx e
തൊഴിലിൽ	t o zh i l i lw
തൊഴിലും	t o zh i l u q
തൊഴിൽ	t o zh i lw
തോന്നാതിരിപ്പാനും	t oo n n aa t i r i p p aa n u q
തോന്നി	t oo n n i
തോൽപ്പിക്കപ്പെടുന്നതും	t oo lw p p i k k a p p e tx u n n a t u q
ത്വൽപുരുഷാ	t w a lw p u r u sx aa
ദിക്കിലെല്ലാം	d i k k i l e l l aa q
ദിക്കിൽ	d i k k i lw
ദിവസം	d i w a s a q
ദിവ്യപുരുഷന്മാരെ	d i w y a p u r u sx a n m aa r e
ദീവട്ടിക്കൊള്ള	d ii w a tx tx i k k o lx lx a
ദുഃഖം	d u hq kh a q
ദുസ്സഹമായിട്ടുള്ളതല്ലേ	d u s s a h a m aa y i tx tx u lx lx a t a l l ee
ദേഹത്തിനല്ല	d ee h a t t i n a l l a
ദേഹപരിശോധന	d ee h a p a r i sh oo dh a n a
ദൈവം	d ai w a q
ദ്രവ്യം	d r a w y a q
ദ്വയീ	d w a y ii
ദ്വേധാ	d w ee dh aa
ന	n a
നടന്ന	n a tx a n n a
നടന്നു	n a tx a n n u
നമ്പൂതിരി	n a m p uu t i r i
നമ്പൂതിരിയുടെ	n a m p uu t i r i y u tx e
നമ്പൂതിരിയെന്നു	n a m p uu t i r i y e n n u
നല്ലത്	n a l l a t
നവീനനും	n a l l a t
നവീനബുദ്ധിയുണ്ടായിരിക്കുന്നത്	n a w ii n a b u d dh i y u nx tx aa y i r i k k u n n a t
നായർ	n aa y a rw
നാരായണീയം	n aa r aa y a nx ii y a q
നാലാം	n aa l aa q
നാലാമച്ഛനായ	n aa l aa m a c ch a n aa y a
നാലാമച്ഛന്	n aa l aa m a c ch a n
നാലാമച്ഛൻ	n aa l aa m a c ch a nn
നാലിൽ	n aa l i lw
നാലു	n aa l u
നികൃന്തതി	n i k rq n t a t i
നിങ്ങളിൽ	n i ng ng a lx i lw
നിങ്ങൾക്കു	n i ng ng a ln k k u
നിങ്ങൾക്കുണ്ടായിരിക്കണം	n i ng ng a ln k k u nx tx aa y i r i k k a nx a q
നിന്നു	n i n n u
നിന്ന്	n i n n
നില	n i l a
നിവൃത്തിയും	n i w rq t t i y u q
നിശ്ചയം	n i sh c a y a q
നിശ്ചയമില്ല	n i sh c a y a m i l l a
നിശ്ചയിച്ചു	n i sh c a y i c c u
നിശ്ചയിച്ച്	n i sh c a y i c c
നിറഭേദമുള്ളത്	n i rx a bh ee d a m u lx lx a t
നേരിടുവാനും	n ee r i tx u w aa n u q
നോക്കട്ടെ	n oo k k a tx tx e
നോക്കി	n oo k k i
നോക്കിനിന്നിരുന്നു	n oo k k i n i n n i r u n n u
നോട്ടം	n oo tx tx a q
നോട്ടുപുസ്തകവും	n oo tx tx u p u s t a k a w u q
ന്യായേന	n y aa y ee n a
പകരക്കാരില്ല	p a k a r a k k aa r i l l a
പകുതിവായയും	p a k u t i w aa y a y u q
പങ്കിട്ടു	p a ng k i tx tx u
പട്ടേരി	p a ng k i tx tx u
പഠിച്ച	p a txh i c c a
പഠിച്ചിട്ടുണ്ട്	p a txh i c c i tx tx u nx tx
പണ്ടു	p a nx tx u
പത്തുകൊല്ലംകൊണ്ട്	p a t t u k o l l a q k o nx tx
പന്ത്രണ്ടടിയും	p a n t r a nx tx a tx i y u q
പന്ത്രണ്ടുതന്നെ	p a n t r a nx tx u t a n n e
പരിവാരങ്ങൾക്ക്	p a r i w aa r a ng ng a ln k k
പരിശോധിച്ചു	p a r i sh oo dh i c c u
പല	p a l a
പലതരങ്ങളും	p a l a t a r a ng ng a lx u q
പലരോടും	p a l a r oo tx u q
പലേടത്തും	p a l ee tx a t t u q
പറഞ്ഞ	p a rx a nj nj a
പറഞ്ഞതുപോലെ	p a rx a nj nj a t u p oo l e
പറഞ്ഞുകൂടാ	p a rx a nj nj u k uu tx aa
പറയത്തക്ക	p a rx a y a t t a k k a
പറയാൻ	p a rx a y aa nn
പറയുന്നതു	p a rx a y u n n a t u
പറയുന്നത്	p a rx a y u n n a t
പറയേണ്ടതായി	p a rx a y ee nx tx a t aa y i
പറവാനുള്ളൂ	p a rx a w aa n u lx lx uu
പറ്റി	p a rx rx i
പറ്റിക്കാതെ	p a rx rx i k k aa t e
പാപം	p aa p a q
പാപമോചനത്തിനും	p aa p a m oo c a n a t t i n u q
പാരമ്പര്യമാഹാത്മ്യത്തെ	p aa r a m p a r y a m aa h aa t m y a t t e
പാലിനു	p aa l i n u
പാലിലിട്ടു	p aa l i l i tx tx u
പാശികളിക്കാരനായിരുന്നു	p aa sh i k a lx i k k aa r a n aa y i r u n n u
പിന്തുടർന്നിരുന്നത്	p i n t u tx a rw n n i r u n n a t
പിന്നെ	p i n n e
പിന്നെയുണ്ടാകുന്ന	p i n n e y u nx tx aa k u n n a
പിറ്റേന്നാൾ	p i rx rx ee n n aa ln
പീടികയിൽ	p ii tx i k a y i lw
പുതിയ	p u t i y a
പുനന്തി	p u n a n t i
പുറത്തിറങ്ങി	p u n a n t i
പുറത്തുകാട്ടി	p u n a n t i
പുറത്തുവന്നിരിക്കുന്നു	p u rx a t t u w a n n i r i k k u n n u
പുറപ്പെടണ്ടാ	p u rx a p p e tx a nx tx aa
പുറപ്പെട്ടത്	p u rx a p p e tx tx a t
പുറപ്പെട്ടു	p u rx a p p e tx tx u
പൂട്ടിയാൽ	p uu tx tx i y aa lw
പൂർണമായി	p uu rw nx a m aa y i
പൂവെച്ചമോതിരം	p uu w e c c a m oo t i r a q
പെട്ടിയിൽ	p e tx tx i y i lw
പേടി	p ee tx i
പേരും	p ee r u q
പേര്	p ee r
പോകുന്നതായാൽ	p oo k u n n a t aa y aa lw
പോക്കറ്റിൽ	p oo k k a rx rx i lw
പോലീസുകാരുടെ	p oo l ii s u k aa r u tx e
പോലീസ്	p oo l ii s
പോലീസ്സുക്കാർക്ക്	p oo l ii s s u k k aa rw k k
പോലെയാകുന്നു	p oo l e y aa k u n n u
പ്രകൃതം	p oo l e y aa k u n n u
പ്രത്യേകം	p r a t y ee k a q
പ്രയത്നക്കുറവല്ലെന്ന്	p r a y a t n a k k u rx a w a l l e n n
പ്രയോഗത്തിന്നു	p r a y oo g a t t i n n u
പ്രയോഗിപ്പാൻ	p r a y oo g i p p aa nn
പ്രവർത്തിച്ചത്	p r a w a rw t t i c c a t
പ്രവർത്തിച്ചു	p r a w a rw t t i c c a t
പ്രവൃത്തിയിൽ	p r a w rq t t i y i lw
പ്രഹരം	p r a h a r a q
പ്രാചീനൻതന്നെ	p r aa c ii n a nn t a n n e
പ്രാപിച്ചത്	p r aa p i c c a t
ബഭാഷിരെ	p r aa p i c c a t
ബാല്യം	b aa l y a q
ബാല്യത്തിൽത്തന്നെ	b aa l y a t t i lw t t a n n e
ബുദ്ധികുറഞ്ഞവരായിരുന്നു	b u d dh i k u rx a nj nj a w a r aa y i r u n n u
ബുദ്ധിമാന്മാരായ	b u d dh i m aa n m aa r aa y a
ബുദ്ധിയുള്ള	b u d dh i y u lx lx a
ഭക്ഷണം	bh a k sx a nx a q
ഭയമുണ്ടായിരുന്നു	bh a y a m u nx tx aa y i r u n n u
ഭവനങ്ങളും	bh a w a n a ng ng a lx u q
ഭാഗ്യഹീനന്മാരായി	bh aa g y a h ii n a n m aa r aa y i
ഭാവം	bh aa w a q
മകനായിരുന്നു	m a k a n aa y i r u n n u
മക്കത്തായ	m a k k a t t aa y a
മടക്കിത്തരുവാനുള്ള	m a tx a k k i t t a r u w aa n u lx lx a
മടങ്ങിപ്പോരികയും	m a tx a ng ng i p p oo r i k a y u q
മടികണ്ടപ്പോൾ	m a tx i k a nx tx a p p oo ln
മടിയനായിരുന്നു	m a tx i k a nx tx a p p oo ln
മതക്കാരനായിരുന്നു	m a t a k k aa r a n aa y i r u n n u
മദിരാശിക്ക്	m a d i r aa sh i k k
മദിരാശിയിൽ	m a d i r aa sh i y i lw
മനസ്സിലാകത്തക്ക	m a n a s s i l aa k a t t a k k a
മനസ്സിലായോ	m a n a s s i l aa y oo
മനുഷ്യൻ	m a n u sx y a nn
മനോരഥം	m a n oo r a th a q
മന്നന്റെ	m a n n a n rx e
മരുന്നു	m a r u n n u
മരുമക്കത്തായ	m a r u m a k k a t t aa y a
മര്യാദക്കാരും	m a r u m a k k a t t aa y a
മര്യാദയ്ക്കാണ്	m a r y aa d a y k k aa nx
മലയാളത്തിൽ	m a l a y aa lx a t t i lw
മഹൻ	m a h a nn
മഹാപാപി	m a h aa p aa p i
മറന്നുപോയി	m a rx a n n u p oo y i
മറ്റാരും	m a rx rx aa r u q
മറ്റു	m a rx rx u
മറ്റേ	m a rx rx ee
മറ്റേത്	m a rx rx ee
മാത്രമായി	m aa t r a m aa y i
മാത്രമേ	m aa t r a m ee
മാസത്തോളം	m aa s a t t oo lx a q
മാറി	m aa rx i
മാറ്റി	m aa rx rx i
മിതിപ്രഭോ	m i t i p r a bh oo
മുഖം	m u kh a q
മുതലിൽ	m u t a l i lw
മുതൽക്കു	m u t a lw k k u
മുതൽക്ക്	m u t a lw k k
മുത്തച്ഛനായിരുന്നു	m u t t a c ch a n aa y i r u n n u
മുത്തച്ഛൻ	m u t t a c ch a nn
മുത്തശ്ശി	m u t t a sh sh i
മുപ്പതുസർഗം	m u p p a t u s a rw g a q
മുമ്പിലത്തെ	m u m p i l a t t e
മുമ്പുതന്നെ	m u m p u t a n n e
മുഴുവനെ	m u zh u w a n e
മുഴുവൻ	m u zh u w a nn
മൂന്നു	m uu n n u
മൃഗങ്ങളായി	m rq g a ng ng a lx aa y i
മൃഗത്തിന്റെ	m rq g a t t i n rx e
മൃഗത്തെയെങ്കിലും	m rq g a t t e y e ng k i l u q
മേനി	m ee n i
മേലിൽ	m ee l i lw
മേൽ	m ee lw
മേശപ്പുറത്തു	m ee sh a p p u rx a t t u
മോതിരം	m oo t i r a q
മോതിരത്തെപ്പറ്റി	m oo t i r a t t e p p a rx rx i
മോതിരവിരലിന്മേൽ	m oo t i r a w i r a l i n m ee lw
മോഹം	m oo h a q
മോഹമില്ലാതെ	m oo h a m i l l aa t e
യാതൊരു	y aa t o r u
യോഗവും	y oo g a w u q
രണ്ടു	r a nx tx u
രണ്ടും	r a nx tx u q
രണ്ടുകൈകൊണ്ടും	r a nx tx u k ai k o nx tx u q
രണ്ടുവഴിക്കും	r a nx tx u w a zh i k k u q
രസം	r a s a q
രസമായില്ലപോൽ	r a s a m aa y i l l a p oo lw
രാജശിക്ഷ	r aa j a sh i k sx a
രാജ്യത്ത്	r aa j y a t t
രാത്രി	r aa t r i
രാത്രിയിൽ	r aa t r i y i lw
രാമൻ	r aa m a nn
ലുനന്തി	l u n a n t i
വകക്കാര്	w a k a k k aa r
വച്ചിരിക്കുന്നു	w a c c i r i k k u n n u
വച്ചിരുന്ന	w a c c i r u n n a
വച്ചിരുന്നതും	w a c c i r u n n a t u q
വച്ച്	w a c c
വൻതരത്തിൽ	w a nn t a r a t t i lw
വന്നതിനാൽ	w a n n a t i n aa lw
വന്നത്	w a n n a t
വന്നിട്ടുള്ളവർ	w a n n i tx tx u lx lx a w a rw
വന്നിരിക്കുന്നു	w a n n i r i k k u n n u
വന്നിരുന്നു	w a n n i r u n n u
വന്നു	w a n n u
വയസ്സു	w a y a s s u
വരരുതെന്നു	w a r a r u t e n n u
വരുത്തുന്ന	w a r u t t u n n a
വരുവാൻ	w a r u w aa nn
വല	w a l a
വലത്തോട്ട്	w a l a t t oo tx tx
വലയിൽ	w a l a y i lw
വലിയ	w a l i y a
വല്ല	w a l l a
വളരെ	w a lx a r e
വഴികളും	w a zh i k a lx u q
വഴിക്കും	w a zh i k a lx u q
വാങ്ങുവാൻ	w aa ng ng u w aa nn
വായിക്കുന്ന	w aa y i k k u n n a
വാസനകൊണ്ട്	w aa s a n a k o nx tx
വാസനയും	w aa s a n a k o nx tx
വാസനാം	w aa s a n aa q
വാസനാബലം	w aa s a n aa b a l a q
വാസനാവികൃതി	w aa s a n aa w i k rq t i
വിചാരിക്കുന്നുള്ളൂ	w i c aa r i k k u n n u lx lx uu
വിചാരിക്കുമ്പോൾ	w i c aa r i k k u m p oo ln
വിചാരിച്ചാണ്	w i c aa r i c c aa nx
വിചാരിച്ചു	w i c aa r i c c u
വിചാരിച്ച്	w i c aa r i c c
വിഡ്‌ഢിത്തം	w i dx dxh i t t a q
വിഡ്‌ഢിത്തത്തിന്റെ	w i dx dxh i t t a t t i n rx e
വിഡ്‌ഢിയുണ്ടെങ്കിൽ	w i dx dxh i y u nx tx e ng k i lw
വിഡ്‌ഢ്യാൻ	w i dx dxh y aa nn
വിദ്യയും	w i d y a y u q
വിദ്യാഭ്യാസവിഷയത്തിൽ	w i d y aa bh y aa s a w i sx a y a t t i lw
വിധമാണ്	w i d y aa bh y aa s a w i sx a y a t t i lw
വിധേനയും	w i dh ee n a y u q
വിലപിടിച്ച	w i l a p i tx i c c a
വിശ്വനാഥദർശനവും	w i sh w a n aa th a d a rw sh a n a w u q
വിഹിതാ	w i h i t aa
വീടായതുകൊണ്ട്	w ii tx aa y a t u k o nx tx
വീട്	w ii tx
വീട്ടിലുള്ളവരിൽ	w ii tx tx i l u lx lx a w a r i lw
വീട്ടിൽനിന്നു	w ii tx tx i lw n i n n u
വീട്ടുന്നതിന്	w ii tx tx u n n a t i n
വെടിവയ്ക്കുവാൻ	w e tx i w a y k k u w aa nn
വെളുത്തും	w e lx u t t u q
വേണ്ടാസനത്തിനു	w ee nx tx aa s a n a t t i n u
വേണ്ടി	w ee nx tx i
വേർതിരിവ്	w ee rw t i r i w
വേർപെടുത്തുവാൻ	w ee rw p e tx u t t u w aa nn
വേറെ	w ee rx e
വേറൊരു	w ee rx o r u
വൈകുന്നേരത്തെ	w ai k u n n ee r a t t e
വൈഷമ്യത്തിലും	w ai k u n n ee r a t t e
വൈഷമ്യവും	w ai sx a m y a w u q
വ്യത്യാസം	w y a t y aa s a q
വ്യസനമായി	w y a s a n a m aa y i
വ്യാഖ്യാനമുണ്ടെങ്കിൽ	w y aa kh y aa n a m u nx tx e ng k i lw
വ്യുൽപ്പത്തി	w y u lw p p a t t i
വ്യുൽപ്പന്നനായിയെന്ന്	w y u lw p p a n n a n aa y i y e n n
വ്രതാദയഃ	w r a t aa d a y a hq
ശട്ടം	sh a tx tx a q
ശബ്‌ദത്തിന്	sh a b d a t t i n
ശരണം	sh a r a nx a q
ശരിയായിരിക്കാം	sh a r i y aa y i r i k k aa q
ശിക്ഷായോഗ്യന്മാരായി	sh i k sx aa y oo g y a n m aa r aa y i
ശ്രുതിസ്മൃതിഭ്യാം	sh r u t i s m rq t i bh y aa q
സംഗതി	s a q g a t i
സംഗതിയുണ്ടെന്നു	s a q g a t i y u nx tx e n n u
സങ്കടം	s a ng k a tx a q
സങ്കടത്തിന്	s a ng k a tx a t t i n
സങ്കടമാണ്	s a ng k a tx a m aa nx
സഞ്ചരിക്കുകയല്ലേ	s a nj c a r i k k u k a y a l l ee
സത്യം	s a t y a q
സന്ധ്യാസമയത്ത്	s a n dh y aa s a m a y a t t
സംബന്ധിച്ചിടത്തോളം	s a q b a n dh i c c i tx a t t oo lx a q
സമയത്ത്	s a m a y a t t
സമീപം	s a m ii p a q
സമ്പാദിച്ചപ്പോഴേക്കും	s a m p aa d i c c a p p oo zh ee k k u q
സമ്പാദിച്ചു	s a m p aa d i c c u
സമ്പാദ്യം	s a m p aa d y a q
സമ്മാനവും	s a m m aa n a w u q
സർഗ്ഗം	s a rw g g a q
സവ്യസാചിത്വവും	s a w y a s aa c i t w a w u q
സംശയം	s a q sh a y a q
സഹപാഠികളിൽ	s a h a p aa txh i k a lx i lw
സഹായം	s a h aa y a q
സഹിക്കാവുന്ന	s a h i k k aa w u n n a
സാക്ഷി	s aa k sx i
സാധനമായാലേ	s aa dh a n a m aa y aa l ee
സാധിക്കുന്നതിന്ന്	s aa dh i k k u n n a t i n n
സാധിച്ചില്ലെങ്കിൽ	s aa dh i c c i l l e ng k i lw
സാമർഥ്യം	s aa m a rw th y a q
സാമർഥ്യമുണ്ടാകണമെങ്കിൽ	s aa m a rw th y a m u nx tx aa k a nx a m e ng k i lw
സാമാനം	s aa m aa n a q
സായ്പിന്റെ	s aa y p i n rx e
സൗഖ്യവും	s au kh y a w u q
സൗന്ദര്യവും	s au n d a r y a w u q
സ്തംഭാകാരമായിട്ടു	s t a q bh aa k aa r a m aa y i tx tx u
സ്ഥലത്താണെന്നു	s th a l a t t aa nx e n n u
സ്‌നേഹിതയായ	s n ee h i t a y aa y a
സ്വപ്നം	s w a p n a q
സ്റ്റേഷനിൽ	s rx rx ee sx a n i lw

!sil	SIL
<unk>	SPN
അകത്തുകടന്ന്	a k a t̪ t̪ u k a ʈ a n̪ n̪
അക്കാലത്ത്	a k k aː l a t̪ t̪
അച്ഛനാണ്	a t͡ʃ t͡ʃʰ a n̪ aː ɳ
അച്ഛനെയല്ല	a t͡ʃ t͡ʃʰ a n̪ e j a l l a
അച്ഛൻ	a t͡ʃ t͡ʃʰ a n
അഞ്ചു	a ɲ t͡ʃ u
അഞ്ചുറുപ്പിക	a ɲ t͡ʃ u r u p p i k a
അഞ്ചെട്ടു	a ɲ t͡ʃ e ʈ ʈ u
അടുത്തകാലത്തിന്നുള്ളിൽ	a ʈ u t̪ t̪ a k aː l a t̪ t̪ i n̪ n̪ u ɭ ɭ i l
അടുത്തുചെന്നു	a ʈ u t̪ t̪ u t͡ʃ e n̪ n̪ u
അതാണ്	a t̪ aː ɳ
അതികേമമായിരുന്നു	a t̪ i k eː m a m aː j i ɾ u n̪ n̪ u
അതിനിടയിൽ	a t̪ i n̪ i ʈ a j i l
അതിപ്രേമമായിരുന്നു	a t̪ i p ɾ eː m a m aː j i ɾ u n̪ n̪ u
അതിൽ	a t̪ i l
അതിസൗഭാഗ്യവതിയായ	a t̪ i s au̯ bʱ aː ɡ j a ʋ a t̪ i j aː j a
അതു	a t̪ u
അതും	a t̪ u m
അതുകൊണ്ട്	a t̪ u k o ɳ ʈ
അതുണ്ടായില്ല	a t̪ u ɳ ʈ aː j i l l a
അതുപോലെ	a t̪ u p oː l e
അത്	a t̪
അത്ര	a t̪ ɾ a
അദ്ദേഹം	a d̪ d̪ eː ɦ a m
അദ്ദേഹത്തിന്റെ	a d̪ d̪ eː ɦ a t̪ t̪ i n ṯ e
അധികം	a d̪ʱ i k a m
അനന്തസേവാ	a n̪ a n̪ t̪ a s eː ʋ aː
അനുഭവം	a n̪ u bʱ a ʋ a m
അനുഭവിക്കുന്നതിൽ	a n̪ u bʱ a ʋ i k k u n̪ n̪ a t̪ i l
അനുഭവിക്കുന്നവരും	a n̪ u bʱ a ʋ i k k u n̪ n̪ a ʋ a ɾ u m
അനുഭവിച്ചവരും	a n̪ u bʱ a ʋ i t͡ʃ t͡ʃ a ʋ a ɾ u m
അനുഭവിച്ചിട്ടുള്ളതിൽ	a n̪ u bʱ a ʋ i t͡ʃ t͡ʃ i ʈ ʈ u ɭ ɭ a t̪ i l
അനുരാഗമുണ്ടായിരുന്നു	a n̪ u ɾ aː ɡ a m u ɳ ʈ aː j i ɾ u n̪ n̪ u
അന്നു	a n̪ n̪ u
അന്വേഷണം	a n̪ ʋ eː ʂ a ɳ a m
അപമാനമില്ല	a p a m aː n̪ a m i l l a
അപമാനമേയുള്ളൂ	a p a m aː n̪ a m eː j u ɭ ɭ uː
അപ്പോൾ	a p p oː ɭ
അപ്പോഴേക്ക്	a p p oː ɻ eː k k
അഭിപ്രായക്കാരനായിരുന്നില്ല	a bʱ i p ɾ aː j a k k aː ɾ a n̪ aː j i ɾ u n̪ n̪ i l l a
അഭ്യസിച്ചിരിക്കണം	a bʱ j a s i t͡ʃ t͡ʃ i ɾ i k k a ɳ a m
അമര്യാദക്കാരുമായിട്ടാണ്	a m a ɾ j aː d̪ a k k aː ɾ u m aː j i ʈ ʈ aː ɳ
അമര്യാദതാവഴിയിലാണ്	a m a ɾ j aː d̪ a t̪ aː ʋ a ɻ i j i l aː ɳ
അമര്യാദതാവഴിയിൽ	a m a ɾ j aː d̪ a t̪ aː ʋ a ɻ i j i l
അമാനുഷൻ	a m aː n̪ u ʂ a n
അമ്മാവനും	a m m aː ʋ a n̪ u m
അയാളെ	a j aː ɭ e
അർജുനന്റെ	a r ɟ u n̪ a n ṯ e
അർഥം	a r t̪ʰ a m
അല്ലേ	a l l eː
അവനായിട്ട്	a ʋ a n̪ aː j i ʈ ʈ
അവന്റെ	a ʋ a n ṯ e
അവമാനം	a ʋ a m aː n̪ a m
അവരിൽ	a ʋ a ɾ i l
അവരുടെ	a ʋ a ɾ u ʈ e
അവശേഷിക്കുന്ന	a ʋ a ʃ eː ʂ i k k u n̪ n̪ a
അവളേയും	a ʋ a ɭ eː j u m
അവൾക്കു	a ʋ a ɭ k k u
അവിടെച്ചെന്നാൽ	a ʋ i ʈ e t͡ʃ t͡ʃ e n̪ n̪ aː l
അശേഷം	a ʃ eː ʂ a m
അസ്വാധീനത്തിങ്കലും	a s ʋ aː d̪ʱ iː n̪ a t̪ t̪ i ŋ k a l u m
അറിയാതെ	a r i j aː t̪ e
അറിയാവുന്നതായിരുന്നാൽ	a r i j aː ʋ u n̪ n̪ a t̪ aː j i ɾ u n̪ n̪ aː l
അറിവാൻ	a r i ʋ aː n
അറിവ്	a r i ʋ
ആ	aː
ആണ്	aː ɳ
ആദ്യം	aː d̪ j a m
ആപത്തിനുള്ള	aː p a t̪ t̪ i n̪ u ɭ ɭ a
ആപത്തുകളെ	aː p a t̪ t̪ u k a ɭ e
ആഭരണപ്പെട്ടി	aː bʱ a ɾ a ɳ a p p e ʈ ʈ i
ആയി	aː j i
ആലോചിച്ചു	aː l oː t͡ʃ i t͡ʃ t͡ʃ u
ആറുമാസവും	aː r u m aː s a ʋ u m
ഇക്കണ്ടകുറുപ്പിന്റെ	i k k a ɳ ʈ a k u r u p p i n ṯ e
ഇക്കണ്ടക്കുറുപ്പ്	i k k a ɳ ʈ a k k u r u p p
ഇങ്ങനെ	i ŋ ŋ a n̪ e
ഇടയുണ്ട്	i ʈ a j u ɳ ʈ
ഇടയ്‌ക്കിടെ	i ʈ a j k k i ʈ e
ഇടുവിച്ചു	i ʈ u ʋ i t͡ʃ t͡ʃ u
ഇട്ടിനാരായണൻ	i ʈ ʈ i n̪ aː ɾ aː j a ɳ a n
ഇട്യാറാണന്റെ	i ʈ j aː r aː ɳ a n ṯ e
ഇട്യാറാണാൻ	i ʈ j aː r aː ɳ aː n
ഇതാണ്	i t̪ aː ɳ
ഇതിലൊന്നിലും	i t̪ i l o n̪ n̪ i l u m
ഇതു	i t̪ u
ഇതുപോലെയാണ്	i t̪ u p oː l e j aː ɳ
ഇതുവരെ	i t̪ u ʋ a ɾ e
ഇത്ര	i t̪ ɾ a
ഇത്രവളരെക്കാലം	i t̪ ɾ a ʋ a ɭ a ɾ e k k aː l a m
ഇദ്ദേഹത്തിനു	i d̪ d̪ eː ɦ a t̪ t̪ i n̪ u
ഇദ്ദേഹത്തിനെ	i d̪ d̪ eː ɦ a t̪ t̪ i n̪ e
ഇനി	i n̪ i
ഇനിക്കവിടെ	i n̪ i k k a ʋ i ʈ e
ഇനിക്കു	i n̪ i k k u
ഇനിക്ക്	i n̪ i k k
ഇന്നലെയും	i n̪ n̪ a l e j u m
ഇന്നും	i n̪ n̪ u m
ഇരിങ്ങാലക്കുട	i ɾ i ŋ ŋ aː l a k k u ʈ a
ഇരിപ്പാൻ	i ɾ i p p aː n
ഇരുന്നാൽ	i ɾ u n̪ n̪ aː l
ഇരുപതു	i ɾ u p a t̪ u
ഇല്ല	i l l a
ഇല്ലത്തെ	i l l a t̪ t̪ e
ഇല്ലാഞ്ഞാൽ	i l l aː ɲ ɲ aː l
ഇല്ലാതെവശായി	i l l aː t̪ e ʋ a ʃ aː j i
ഇല്ലെന്നല്ല	i l l e n̪ n̪ a l l a
ഇവനു	i ʋ a n̪ u
ഇവനെ	i ʋ a n̪ e
ഇവിടെ	i ʋ i ʈ e
ഈ	iː
ഈരാറു	iː ɾ aː r u
ഉച്ചതിരിഞ്ഞ	u t͡ʃ t͡ʃ a t̪ i ɾ i ɲ ɲ a
ഉടനെ	u ʈ a n̪ e
ഉണരാതിരിപ്പാൻ	u ɳ a ɾ aː t̪ i ɾ i p p aː n
ഉണരാത്ത	u ɳ a ɾ aː t̪ t̪ a
ഉണരുന്നത്	u ɳ a ɾ u n̪ n̪ a t̪
ഉണരുമോ	u ɳ a ɾ u m oː
ഉണർന്നു	u ɳ a r n̪ n̪ u
ഉണ്ടായി	u ɳ ʈ aː j i
ഉണ്ടായിട്ടില്ല	u ɳ ʈ aː j i ʈ ʈ i l l a
ഉണ്ടായിരുന്നില്ല	u ɳ ʈ aː j i ɾ u n̪ n̪ i l l a
ഉണ്ടായില്ല	u ɳ ʈ aː j i l l a
ഉണ്ടെന്നു	u ɳ ʈ e n̪ n̪ u
ഉണ്ട്	u ɳ ʈ
ഉത്സാഹിച്ചു	u t̪ s aː ɦ i t͡ʃ t͡ʃ u
ഉദ്യോഗസ്ഥന്മാരാൽ	u d̪ j oː ɡ a s t̪ʰ a n̪ m aː ɾ aː l
ഉദ്യോഗസ്ഥന്മാർക്ക്	u d̪ j oː ɡ a s t̪ʰ a n̪ m aː r k k
ഉള്ളതാണ്	u ɭ ɭ a t̪ aː ɳ
ഉറക്കമാണ്	u r a k k a m aː ɳ
ഉറങ്ങിയിരുന്നത്	u r a ŋ ŋ i j i ɾ u n̪ n̪ a t̪
ഊരാഞ്ചാടിയായിരുന്നാലും	uː ɾ aː ɲ t͡ʃ aː ʈ i j aː j i ɾ u n̪ n̪ aː l u m
ഊരാറില്ല	uː ɾ aː r i l l a
എങ്കിലും	e ŋ k i l u m
എങ്ങനെയാണെന്നു	e ŋ ŋ a n̪ e j aː ɳ e n̪ n̪ u
എങ്ങനെയാണ്	e ŋ ŋ a n̪ e j aː ɳ
എടത്തെ	e ʈ a t̪ t̪ e
എടത്തെക്കൈയിന്റെ	e ʈ a t̪ t̪ e k k ai̯ j i n ṯ e
എടുക്കണമെന്നുണ്ടായിരുന്നില്ല	e ʈ u k k a ɳ a m e n̪ n̪ u ɳ ʈ aː j i ɾ u n̪ n̪ i l l a
എടുത്ത	e ʈ u t̪ t̪ a
എടുത്ത്	e ʈ u t̪ t̪
എനിക്കിട്ടിട്ടുള്ളതും	e n̪ i k k i ʈ ʈ i ʈ ʈ u ɭ ɭ a t̪ u m
എനിക്കു	e n̪ i k k u
എനിക്കുണ്ടായി	e n̪ i k k u ɳ ʈ aː j i
എനിക്കുണ്ടായില്ല	e n̪ i k k u ɳ ʈ aː j i l l a
എനിക്ക്	e n̪ i k k
എന്ന	e n̪ n̪ a
എന്നാൽ	e n̪ n̪ aː l
എന്നിങ്ങനെ	e n̪ n̪ i ŋ ŋ a n̪ e
എന്നിട്ടാണ്	e n̪ n̪ i ʈ ʈ aː ɳ
എന്നു	e n̪ n̪ u
എന്നുമാത്രമല്ല	e n̪ n̪ u m aː t̪ ɾ a m a l l a
എന്നുലള്ളതിലേക്ക്	e n̪ n̪ u l a ɭ ɭ a t̪ i l eː k k
എന്നെ	e n̪ n̪ e
എന്നെക്കാൾ	e n̪ n̪ e k k aː ɭ
എന്നെപ്പോലെ	e n̪ n̪ e p p oː l e
എന്നേയും	e n̪ n̪ eː j u m
എന്ന്	e n̪ n̪
എന്റെ	e n ṯ e
എല്ലാ	e l l aː
എല്ലാം	e l l aː m
എല്ലാവരും	e l l aː ʋ a ɾ u m
എവിടെപ്പോയിരിക്കാമെന്ന്	e ʋ i ʈ e p p oː j i ɾ i k k aː m e n̪ n̪
എഴുതുന്നില്ല	e ɻ u t̪ u n̪ n̪ i l l a
എഴുന്നേറ്റ്	e ɻ u n̪ n̪ eː ṯ ṯ
ഏകസംബന്ധിജ്ഞാനമപരസംബന്ധിസ്‌മാരകമെന്ന	eː k a s a m b a n̪ d̪ʱ i ɟ ɲ aː n̪ a m a p a ɾ a s a m b a n̪ d̪ʱ i s m aː ɾ a k a m e n̪ n̪ a
ഏർപ്പെടുത്തി	eː r p p e ʈ u t̪ t̪ i
ഒതുക്കാവുന്നതെല്ലാം	o t̪ u k k aː ʋ u n̪ n̪ a t̪ e l l aː m
ഒന്നു	o n̪ n̪ u
ഒന്ന്	o n̪ n̪
ഒപ്പ്	o p p
ഒരതിരും	o ɾ a t̪ i ɾ u m
ഒരിക്കലും	o ɾ i k k a l u m
ഒരില്ലത്താണ്	o ɾ i l l a t̪ t̪ aː ɳ
ഒരു	o ɾ u
ഒരുപോലെ	o ɾ u p oː l e
ഒരുവകക്കാര്	o ɾ u ʋ a k a k k aː ɾ
ഒരുവിധം	o ɾ u ʋ i d̪ʱ a m
ഒഴിഞ്ഞുപോകണമെന്നു	o ɻ i ɲ ɲ u p oː k a ɳ a m e n̪ n̪ u
ഒറ്റയ്ക്കുപോയി	o ṯ ṯ a j k k u p oː j i
ഒറ്റയ്ക്കുള്ളതായിരിക്കുകയാണ്	o ṯ ṯ a j k k u ɭ ɭ a t̪ aː j i ɾ i k k u k a j aː ɳ
ഒറ്റ്	o ṯ ṯ
ഓർമവന്നപ്പോൾ	oː r m a ʋ a n̪ n̪ a p p oː ɭ
ഓർമവന്നു	oː r m a ʋ a n̪ n̪ u
ഓർമ്മയ്‌ക്കു	oː r m m a j k k u
ഓഹരിയും	oː ɦ a ɾ i j u m
കക്കാറും	k a k k aː r u m
കക്കുക	k a k k u k a
കടം	k a ʈ a m
കണ്ടപ്പോൾ	k a ɳ ʈ a p p oː ɭ
കണ്ടപ്പോൾത്തന്നെ	k a ɳ ʈ a p p oː ɭ t̪ t̪ a n̪ n̪ e
കണ്ടിട്ടുള്ള	k a ɳ ʈ i ʈ ʈ u ɭ ɭ a
കണ്ടില്ല	k a ɳ ʈ i l l a
കണ്ടുനിന്നു	k a ɳ ʈ u n̪ i n̪ n̪ u
കണ്ടെത്താതിരിക്കയില്ല	k a ɳ ʈ e t̪ t̪ aː t̪ i ɾ i k k a j i l l a
കണ്ടെത്തിക്കിട്ടായാൽ	k a ɳ ʈ e t̪ t̪ i k k i ʈ ʈ aː j aː l
കണ്ടെത്തിയാൽ	k a ɳ ʈ e t̪ t̪ i j aː l
കണ്ട്	k a ɳ ʈ
കഥ	k a t̪ʰ a
കഥയില്ലായ്മ	k a t̪ʰ a j i l l aː j m a
കയ്‌പീത്തുകൊടുക്കാം	k a j p iː t̪ t̪ u k o ʈ u k k aː m
കയ്യിട്ടു	k a j j i ʈ ʈ u
കയ്യിലെടുത്തു	k a j j i l e ʈ u t̪ t̪ u
കയ്യിൽ	k a j j i l
കരുതി	k a ɾ u t̪ i
കരുതിയാണ്	k a ɾ u t̪ i j aː ɳ
കല്യാണിക്കുട്ടിയെ	k a l j aː ɳ i k k u ʈ ʈ i j e
കല്ല്യാണിക്കുട്ടിക്കു	k a l l j aː ɳ i k k u ʈ ʈ i k k u
കഷ്‌ടമല്ല	k a ʂ ʈ a m a l l a
കളവിൽ	k a ɭ a ʋ i l
കളവു	k a ɭ a ʋ u
കളവുകവിഞ്ഞതിൽ	k a ɭ a ʋ u k a ʋ i ɲ ɲ a t̪ i l
കളവുകളും	k a ɭ a ʋ u k a ɭ u m
കളവുണ്ടായത്	k a ɭ a ʋ u ɳ ʈ aː j a t̪
കളവുനടത്തി	k a ɭ a ʋ u n̪ a ʈ a t̪ t̪ i
കളവുപോയി	k a ɭ a ʋ u p oː j i
കളവുവിട്ട്	k a ɭ a ʋ u ʋ i ʈ ʈ
കളവ്	k a ɭ a ʋ
കള്ളനാവാനുള്ള	k a ɭ ɭ a n̪ aː ʋ aː n̪ u ɭ ɭ a
കള്ളൻ	k a ɭ ɭ a n
കഴിച്ച്	k a ɻ i t͡ʃ t͡ʃ
കഴിഞ്ഞപ്പോഴേക്കും	k a ɻ i ɲ ɲ a p p oː ɻ eː k k u m
കഴിഞ്ഞ്	k a ɻ i ɲ ɲ
കഴിയില്ലെന്നു	k a ɻ i j i l l e n̪ n̪ u
കറപറ്റിയ	k a r a p a ṯ ṯ i j a
കറുത്തും	k a r u t̪ t̪ u m
കറുപ്പുകൂടിയ	k a r u p p u k uː ʈ i j a
കാടരികായിട്ടുള്ള	k aː ʈ a ɾ i k aː j i ʈ ʈ u ɭ ɭ a
കാടരികിൽ	k aː ʈ a ɾ i k i l
കാട്ടിൽപോകുവാനും	k aː ʈ ʈ i l p oː k u ʋ aː n̪ u m
കാരണവന്മാരുടെ	k aː ɾ a ɳ a ʋ a n̪ m aː ɾ u ʈ e
കാലത്തും	k aː l a t̪ t̪ u m
കാലത്തുതന്നെ	k aː l a t̪ t̪ u t̪ a n̪ n̪ e
കാലത്തെ	k aː l a t̪ t̪ e
കാലത്തേ	k aː l a t̪ t̪ eː
കാവ്യം	k aː ʋ j a m
കാഴ്‌ച	k aː ɻ t͡ʃ a
കിടന്നുറങ്ങുമ്പോൾ	k i ʈ a n̪ n̪ u r a ŋ ŋ u m p oː ɭ
കിട്ടണമെന്നാണെന്നു	k i ʈ ʈ a ɳ a m e n̪ n̪ aː ɳ e n̪ n̪ u
കിട്ടിയെന്ന്	k i ʈ ʈ i j e n̪ n̪
കിട്ടീട്ടുള്ള	k i ʈ ʈ iː ʈ ʈ u ɭ ɭ a
കിട്ടുന്ന	k i ʈ ʈ u n̪ n̪ a
കിട്ടുവാൻ	k i ʈ ʈ u ʋ aː n
കുടുങ്ങുന്ന	k u ʈ u ŋ ŋ u n̪ n̪ a
കുട്ടികൾക്കുകൂടി	k u ʈ ʈ i k a ɭ k k u k uː ʈ i
കുറച്ചു	k u r a t͡ʃ t͡ʃ u
കുറച്ചുദിവസത്തേക്ക്	k u r a t͡ʃ t͡ʃ u d̪ i ʋ a s a t̪ t̪ eː k k
കുറെ	k u r e
കൂടാതെ	k uː ʈ aː t̪ e
കൂടി	k uː ʈ i
കൂട്ടത്തിലേക്ക്	k uː ʈ ʈ a t̪ t̪ i l eː k k
കെട്ടി	k e ʈ ʈ i
കെട്ടിയിരുന്നത്	k e ʈ ʈ i j i ɾ u n̪ n̪ a t̪
കെണിയാണെന്ന്	k e ɳ i j aː ɳ e n̪ n̪
കേട്ടിരിക്കാതിരിക്കയില്ല	k eː ʈ ʈ i ɾ i k k aː t̪ i ɾ i k k a j i l l a
കേൾക്കാത്ത	k eː ɭ k k aː t̪ t̪ a
കൈക്കലാക്കി	k ai̯ k k a l aː k k i
കൈയിന്മേൽ	k ai̯ j i n̪ m eː l
കൈയ്യിൽ	k ai̯ j j i l
കൈവശത്തിൽ	k ai̯ ʋ a ʃ a t̪ t̪ i l
കൈവിലങ്ങും	k ai̯ ʋ i l a ŋ ŋ u m
കൊച്ചി	k o t͡ʃ t͡ʃ i
കൊച്ചിശ്ശീമയിലാണ്	k o t͡ʃ t͡ʃ i ʃ ʃ iː m a j i l aː ɳ
കൊടുക്കാവു	k o ʈ u k k aː ʋ u
കൊടുങ്ങല്ലൂർ	k o ʈ u ŋ ŋ a l l uː r
കൊടുത്ത	k o ʈ u t̪ t̪ a
കൊടുത്തിട്ടുണ്ടായിരുന്നു	k o ʈ u t̪ t̪ i ʈ ʈ u ɳ ʈ aː j i ɾ u n̪ n̪ u
കൊടുത്തു	k o ʈ u t̪ t̪ u
കൊണ്ടുപിടിച്ചു	k o ɳ ʈ u p i ʈ i t͡ʃ t͡ʃ u
കൊല്ലത്തോളം	k o l l a t̪ t̪ oː ɭ a m
കൊള്ളരുതാത്ത	k o ɭ ɭ a ɾ u t̪ aː t̪ t̪ a
കൊള്ളാറും	k o ɭ ɭ aː r u m
കോടതി	k oː ʈ a t̪ i
കോടതിപൂട്ടൽപ്പോലെ	k oː ʈ a t̪ i p uː ʈ ʈ a l p p oː l e
കോണം	k oː ɳ a m
കോൺസ്റ്റബിൾ	k oː ɳ s ṯ ṯ a b i ɭ
കോന്ത്രമ്പല്ലും	k oː n̪ t̪ ɾ a m p a l l u m
ഗംഗാസ്നാനവും	ɡ a m ɡ aː s n̪ aː n̪ a ʋ u m
ഗണാഷ്‌ടകവ്യുൽപ്പത്തി	ɡ a ɳ aː ʂ ʈ a k a ʋ j u l p p a t̪ t̪ i
ഗന്തർ	ɡ a n̪ t̪ a r
ഗംഭീരന്മാർ	ɡ a m bʱ iː ɾ a n̪ m aː r
ഗുജിലിത്തെരുവിൽ	ɡ u ɟ i l i t̪ t̪ e ɾ u ʋ i l
ഗുരുനാഥൻ	ɡ u ɾ u n̪ aː t̪ʰ a n
ഗൃഹസ്ഥന്റെ	ɡ rɨ ɦ a s t̪ʰ a n ṯ e
ഗ്രാമക്കാർ	ɡ ɾ aː m a k k aː r
ചാടിപ്പോന്നതിൽപ്പിന്നെ	t͡ʃ aː ʈ i p p oː n̪ n̪ a t̪ i l p p i n̪ n̪ e
ചിലരെങ്കിലും	t͡ʃ i l a ɾ e ŋ k i l u m
ചിലർ	t͡ʃ i l a r
ചില്ലറ	t͡ʃ i l l a r a
ചീത്തയാണെന്നല്ലേ	t͡ʃ iː t̪ t̪ a j aː ɳ e n̪ n̪ a l l eː
ചുരുക്കമായിരിക്കും	t͡ʃ u ɾ u k k a m aː j i ɾ i k k u m
ചെന്ന	t͡ʃ e n̪ n̪ a
ചെന്നപ്പോൾ	t͡ʃ e n̪ n̪ a p p oː ɭ
ചെന്നുചാടുന്നത്	t͡ʃ e n̪ n̪ u t͡ʃ aː ʈ u n̪ n̪ a t̪
ചെന്ന്	t͡ʃ e n̪ n̪
ചെയ്‌ത	t͡ʃ e j t̪ a
ചെയ്‌തിട്ടുണ്ടായിരുന്നു	t͡ʃ e j t̪ i ʈ ʈ u ɳ ʈ aː j i ɾ u n̪ n̪ u
ചെയ്തു	t͡ʃ e j t̪ u
ചെയ്‌ത്	t͡ʃ e j t̪
ചെയ്യട്ടെ	t͡ʃ e j j a ʈ ʈ e
ചെയ്യുന്നത്	t͡ʃ e j j u n̪ n̪ a t̪
ചെയ്വാൻ	t͡ʃ e j ʋ aː n
ചെല്ലുകയുള്ളൂ	t͡ʃ e l l u k a j u ɭ ɭ uː
ചൊല്ലാറുണ്ട്	t͡ʃ o l l aː r u ɳ ʈ
ചോടു	t͡ʃ oː ʈ u
ചോദിക്കയും	t͡ʃ oː d̪ i k k a j u m
ചോദിച്ചപ്പോൾ	t͡ʃ oː d̪ i t͡ʃ t͡ʃ a p p oː ɭ
ജനത്തിരിക്കും	ɟ a n̪ a t̪ t̪ i ɾ i k k u m
ജനനം	ɟ a n̪ a n̪ a m
ജാതി	ɟ aː t̪ i
ഞങ്ങളുടെ	ɲ a ŋ ŋ a ɭ u ʈ e
ഞാനിതാ	ɲ aː n̪ i t̪ aː
ഞാനും	ɲ aː n̪ u m
ഞാനെന്റെ	ɲ aː n̪ e n ṯ e
ഞാനൊരു	ɲ aː n̪ o ɾ u
ഞാൻ	ɲ aː n
ഞെട്ടി	ɲ e ʈ ʈ i
തടസ്സവും	t̪ a ʈ a s s a ʋ u m
തട്ടണമെന്ന്	t̪ a ʈ ʈ a ɳ a m e n̪ n̪
തന്നെ	t̪ a n̪ n̪ e
തന്നെയാകുന്നു	t̪ a n̪ n̪ e j aː k u n̪ n̪ u
തന്നെയാണ്	t̪ a n̪ n̪ e j aː ɳ
തന്റെ	t̪ a n ṯ e
തപ്പിനോക്കിയപ്പോൾ	t̪ a p p i n̪ oː k k i j a p p oː ɭ
തമ്മിലുള്ള	t̪ a m m i l u ɭ ɭ a
തരമാകുന്നത്	t̪ a ɾ a m aː k u n̪ n̪ a t̪
തരമില്ലെന്നുതോന്നി	t̪ a ɾ a m i l l e n̪ n̪ u t̪ oː n̪ n̪ i
തലമുറ	t̪ a l a m u r a
തലയ്ക്കൽ	t̪ a l a j k k a l
തലേ	t̪ a l eː
തലേക്കെട്ടും	t̪ a l eː k k e ʈ ʈ u m
തൽക്കാലം	t̪ a l k k aː l a m
തറവാട്ടിലും	t̪ a r a ʋ aː ʈ ʈ i l u m
തറവാട്ടിൽ	t̪ a r a ʋ aː ʈ ʈ i l
താക്കീതു	t̪ aː k k iː t̪ u
താൻ	t̪ aː n
താമസവും	t̪ aː m a s a ʋ u m
താമസിക്കുന്നേടത്തു	t̪ aː m a s i k k u n̪ n̪ eː ʈ a t̪ t̪ u
താവഴിക്കാർ	t̪ aː ʋ a ɻ i k k aː r
താവഴിയും	t̪ aː ʋ a ɻ i j u m
തിരുമനസ്സുകൊണ്ട്	t̪ i ɾ u m a n̪ a s s u k o ɳ ʈ
തീർന്നു	t̪ iː r n̪ n̪ u
തു	t̪ u
തുടങ്ങി	t̪ u ʈ a ŋ ŋ i
തുടങ്ങിയതല്ല	t̪ u ʈ a ŋ ŋ i j a t̪ a l l a
തുമ്പും	t̪ u m p u m
തുറന്ന്	t̪ u r a n̪ n̪
തൃശ്ശിവപേരൂർക്ക്	t̪ rɨ ʃ ʃ i ʋ a p eː ɾ uː r k k
തെണ്ടി	t̪ e ɳ ʈ i
തെണ്ടിനായാട്ടും	t̪ e ɳ ʈ i n̪ aː j aː ʈ ʈ u m
തെളിനായാട്ടായാൽ	t̪ e ɭ i n̪ aː j aː ʈ ʈ aː j aː l
തെളിനായാട്ടും	t̪ e ɭ i n̪ aː j aː ʈ ʈ u m
തെറ്റിപ്പോകുവാൻ	t̪ e ṯ ṯ i p p oː k u ʋ aː n
തേവിടിശ്ശി	t̪ eː ʋ i ʈ i ʃ ʃ i
തേവിടിശ്ശിയുടെ	t̪ eː ʋ i ʈ i ʃ ʃ i j u ʈ e
തൊഴിലിൽ	t̪ o ɻ i l i l
തൊഴിലും	t̪ o ɻ i l u m
തൊഴിൽ	t̪ o ɻ i l
തോന്നാതിരിപ്പാനും	t̪ oː n̪ n̪ aː t̪ i ɾ i p p aː n̪ u m
തോന്നി	t̪ oː n̪ n̪ i
തോൽപ്പിക്കപ്പെടുന്നതും	t̪ oː l p p i k k a p p e ʈ u n̪ n̪ a t̪ u m
ത്വൽപുരുഷാ	t̪ ʋ a l p u ɾ u ʂ aː
ദിക്കിലെല്ലാം	d̪ i k k i l e l l aː m
ദിക്കിൽ	d̪ i k k i l
ദിവസം	d̪ i ʋ a s a m
ദിവ്യപുരുഷന്മാരെ	d̪ i ʋ j a p u ɾ u ʂ a n̪ m aː ɾ e
ദീവട്ടിക്കൊള്ള	d̪ iː ʋ a ʈ ʈ i k k o ɭ ɭ a
ദുഃഖം	d̪ u kʰ a m
ദുസ്സഹമായിട്ടുള്ളതല്ലേ	d̪ u s s a ɦ a m aː j i ʈ ʈ u ɭ ɭ a t̪ a l l eː
ദേഹത്തിനല്ല	d̪ eː ɦ a t̪ t̪ i n̪ a l l a
ദേഹപരിശോധന	d̪ eː ɦ a p a ɾ i ʃ oː d̪ʱ a n̪ a
ദൈവം	d̪ ai̯ ʋ a m
ദ്രവ്യം	d̪ ɾ a ʋ j a m
ദ്വയീ	d̪ ʋ a j iː
ദ്വേധാ	d̪ ʋ eː d̪ʱ aː
ന	n̪ a
നടന്ന	n̪ a ʈ a n̪ n̪ a
നടന്നു	n̪ a ʈ a n̪ n̪ u
നമ്പൂതിരി	n̪ a m p uː t̪ i ɾ i
നമ്പൂതിരിയുടെ	n̪ a m p uː t̪ i ɾ i j u ʈ e
നമ്പൂതിരിയെന്നു	n̪ a m p uː t̪ i ɾ i j e n̪ n̪ u
നല്ലത്	n̪ a l l a t̪
നവീനനും	n̪ a ʋ iː n̪ a n̪ u m
നവീനബുദ്ധിയുണ്ടായിരിക്കുന്നത്	n̪ a ʋ iː n̪ a b u d̪ d̪ʱ i j u ɳ ʈ aː j i ɾ i k k u n̪ n̪ a t̪
നായർ	n̪ aː j a r
നാരായണീയം	n̪ aː ɾ aː j a ɳ iː j a m
നാലാം	n̪ aː l aː m
നാലാമച്ഛനായ	n̪ aː l aː m a t͡ʃ t͡ʃʰ a n̪ aː j a
നാലാമച്ഛന്	n̪ aː l aː m a t͡ʃ t͡ʃʰ a n̪
നാലാമച്ഛൻ	n̪ aː l aː m a t͡ʃ t͡ʃʰ a n
നാലിൽ	n̪ aː l i l
നാലു	n̪ aː l u
നികൃന്തതി	n̪ i k rɨ n̪ t̪ a t̪ i
നിങ്ങളിൽ	n̪ i ŋ ŋ a ɭ i l
നിങ്ങൾക്കു	n̪ i ŋ ŋ a ɭ k k u
നിങ്ങൾക്കുണ്ടായിരിക്കണം	n̪ i ŋ ŋ a ɭ k k u ɳ ʈ aː j i ɾ i k k a ɳ a m
നിന്നു	n̪ i n̪ n̪ u
നിന്ന്	n̪ i n̪ n̪
നില	n̪ i l a
നിവൃത്തിയും	n̪ i ʋ rɨ t̪ t̪ i j u m
നിശ്ചയം	n̪ i ʃ t͡ʃ a j a m
നിശ്ചയമില്ല	n̪ i ʃ t͡ʃ a j a m i l l a
നിശ്ചയിച്ചു	n̪ i ʃ t͡ʃ a j i t͡ʃ t͡ʃ u
നിശ്ചയിച്ച്	n̪ i ʃ t͡ʃ a j i t͡ʃ t͡ʃ
നിറഭേദമുള്ളത്	n̪ i r a bʱ eː d̪ a m u ɭ ɭ a t̪
നേരിടുവാനും	n̪ eː ɾ i ʈ u ʋ aː n̪ u m
നോക്കട്ടെ	n̪ oː k k a ʈ ʈ e
നോക്കി	n̪ oː k k i
നോക്കിനിന്നിരുന്നു	n̪ oː k k i n̪ i n̪ n̪ i ɾ u n̪ n̪ u
നോട്ടം	n̪ oː ʈ ʈ a m
നോട്ടുപുസ്തകവും	n̪ oː ʈ ʈ u p u s t̪ a k a ʋ u m
ന്യായേന	n̪ j aː j eː n̪ a
പകരക്കാരില്ല	p a k a ɾ a k k aː ɾ i l l a
പകുതിവായയും	p a k u t̪ i ʋ aː j a j u m
പങ്കിട്ടു	p a ŋ k i ʈ ʈ u
പട്ടേരി	p a ʈ ʈ eː ɾ i
പഠിച്ച	p a ʈʰ i t͡ʃ t͡ʃ a
പഠിച്ചിട്ടുണ്ട്	p a ʈʰ i t͡ʃ t͡ʃ i ʈ ʈ u ɳ ʈ
പണ്ടു	p a ɳ ʈ u
പത്തുകൊല്ലംകൊണ്ട്	p a t̪ t̪ u k o l l a m k o ɳ ʈ
പന്ത്രണ്ടടിയും	p a n̪ t̪ ɾ a ɳ ʈ a ʈ i j u m
പന്ത്രണ്ടുതന്നെ	p a n̪ t̪ ɾ a ɳ ʈ u t̪ a n̪ n̪ e
പരിവാരങ്ങൾക്ക്	p a ɾ i ʋ aː ɾ a ŋ ŋ a ɭ k k
പരിശോധിച്ചു	p a ɾ i ʃ oː d̪ʱ i t͡ʃ t͡ʃ u
പല	p a l a
പലതരങ്ങളും	p a l a t̪ a ɾ a ŋ ŋ a ɭ u m
പലരോടും	p a l a ɾ oː ʈ u m
പലേടത്തും	p a l eː ʈ a t̪ t̪ u m
പറഞ്ഞ	p a r a ɲ ɲ a
പറഞ്ഞതുപോലെ	p a r a ɲ ɲ a t̪ u p oː l e
പറഞ്ഞുകൂടാ	p a r a ɲ ɲ u k uː ʈ aː
പറയത്തക്ക	p a r a j a t̪ t̪ a k k a
പറയാൻ	p a r a j aː n
പറയുന്നതു	p a r a j u n̪ n̪ a t̪ u
പറയുന്നത്	p a r a j u n̪ n̪ a t̪
പറയേണ്ടതായി	p a r a j eː ɳ ʈ a t̪ aː j i
പറവാനുള്ളൂ	p a r a ʋ aː n̪ u ɭ ɭ uː
പറ്റി	p a ṯ ṯ i
പറ്റിക്കാതെ	p a ṯ ṯ i k k aː t̪ e
പാപം	p aː p a m
പാപമോചനത്തിനും	p aː p a m oː t͡ʃ a n̪ a t̪ t̪ i n̪ u m
പാരമ്പര്യമാഹാത്മ്യത്തെ	p aː ɾ a m p a ɾ j a m aː ɦ aː t̪ m j a t̪ t̪ e
പാലിനു	p aː l i n̪ u
പാലിലിട്ടു	p aː l i l i ʈ ʈ u
പാശികളിക്കാരനായിരുന്നു	p aː ʃ i k a ɭ i k k aː ɾ a n̪ aː j i ɾ u n̪ n̪ u
പിന്തുടർന്നിരുന്നത്	p i n̪ t̪ u ʈ a r n̪ n̪ i ɾ u n̪ n̪ a t̪
പിന്നെ	p i n̪ n̪ e
പിന്നെയുണ്ടാകുന്ന	p i n̪ n̪ e j u ɳ ʈ aː k u n̪ n̪ a
പിറ്റേന്നാൾ	p i ṯ ṯ eː n̪ n̪ aː ɭ
പീടികയിൽ	p iː ʈ i k a j i l
പുതിയ	p u t̪ i j a
പുനന്തി	p u n̪ a n̪ t̪ i
പുറത്തിറങ്ങി	p u r a t̪ t̪ i r a ŋ ŋ i
പുറത്തുകാട്ടി	p u r a t̪ t̪ u k aː ʈ ʈ i
പുറത്തുവന്നിരിക്കുന്നു	p u r a t̪ t̪ u ʋ a n̪ n̪ i ɾ i k k u n̪ n̪ u
പുറപ്പെടണ്ടാ	p u r a p p e ʈ a ɳ ʈ aː
പുറപ്പെട്ടത്	p u r a p p e ʈ ʈ a t̪
പുറപ്പെട്ടു	p u r a p p e ʈ ʈ u
പൂട്ടിയാൽ	p uː ʈ ʈ i j aː l
പൂർണമായി	p uː r ɳ a m aː j i
പൂവെച്ചമോതിരം	p uː ʋ e t͡ʃ t͡ʃ a m oː t̪ i ɾ a m
പെട്ടിയിൽ	p e ʈ ʈ i j i l
പേടി	p eː ʈ i
പേരും	p eː ɾ u m
പേര്	p eː ɾ
പോകുന്നതായാൽ	p oː k u n̪ n̪ a t̪ aː j aː l
പോക്കറ്റിൽ	p oː k k a ṯ ṯ i l
പോലീസുകാരുടെ	p oː l iː s u k aː ɾ u ʈ e
പോലീസ്	p oː l iː s
പോലീസ്സുക്കാർക്ക്	p oː l iː s s u k k aː r k k
പോലെയാകുന്നു	p oː l e j aː k u n̪ n̪ u
പ്രകൃതം	p ɾ a k rɨ t̪ a m
പ്രത്യേകം	p ɾ a t̪ j eː k a m
പ്രയത്നക്കുറവല്ലെന്ന്	p ɾ a j a t̪ n̪ a k k u r a ʋ a l l e n̪ n̪
പ്രയോഗത്തിന്നു	p ɾ a j oː ɡ a t̪ t̪ i n̪ n̪ u
പ്രയോഗിപ്പാൻ	p ɾ a j oː ɡ i p p aː n
പ്രവർത്തിച്ചത്	p ɾ a ʋ a r t̪ t̪ i t͡ʃ t͡ʃ a t̪
പ്രവർത്തിച്ചു	p ɾ a ʋ a r t̪ t̪ i t͡ʃ t͡ʃ u
പ്രവൃത്തിയിൽ	p ɾ a ʋ rɨ t̪ t̪ i j i l
പ്രഹരം	p ɾ a ɦ a ɾ a m
പ്രാചീനൻതന്നെ	p ɾ aː t͡ʃ iː n̪ a n t̪ a n̪ n̪ e
പ്രാപിച്ചത്	p ɾ aː p i t͡ʃ t͡ʃ a t̪
ബഭാഷിരെ	b a bʱ aː ʂ i ɾ e
ബാല്യം	b aː l j a m
ബാല്യത്തിൽത്തന്നെ	b aː l j a t̪ t̪ i l t̪ t̪ a n̪ n̪ e
ബുദ്ധികുറഞ്ഞവരായിരുന്നു	b u d̪ d̪ʱ i k u r a ɲ ɲ a ʋ a ɾ aː j i ɾ u n̪ n̪ u
ബുദ്ധിമാന്മാരായ	b u d̪ d̪ʱ i m aː n̪ m aː ɾ aː j a
ബുദ്ധിയുള്ള	b u d̪ d̪ʱ i j u ɭ ɭ a
ഭക്ഷണം	bʱ a k ʂ a ɳ a m
ഭയമുണ്ടായിരുന്നു	bʱ a j a m u ɳ ʈ aː j i ɾ u n̪ n̪ u
ഭവനങ്ങളും	bʱ a ʋ a n̪ a ŋ ŋ a ɭ u m
ഭാഗ്യഹീനന്മാരായി	bʱ aː ɡ j a ɦ iː n̪ a n̪ m aː ɾ aː j i
ഭാവം	bʱ aː ʋ a m
മകനായിരുന്നു	m a k a n̪ aː j i ɾ u n̪ n̪ u
മക്കത്തായ	m a k k a t̪ t̪ aː j a
മടക്കിത്തരുവാനുള്ള	m a ʈ a k k i t̪ t̪ a ɾ u ʋ aː n̪ u ɭ ɭ a
മടങ്ങിപ്പോരികയും	m a ʈ a ŋ ŋ i p p oː ɾ i k a j u m
മടികണ്ടപ്പോൾ	m a ʈ i k a ɳ ʈ a p p oː ɭ
മടിയനായിരുന്നു	m a ʈ i j a n̪ aː j i ɾ u n̪ n̪ u
മതക്കാരനായിരുന്നു	m a t̪ a k k aː ɾ a n̪ aː j i ɾ u n̪ n̪ u
മദിരാശിക്ക്	m a d̪ i ɾ aː ʃ i k k
മദിരാശിയിൽ	m a d̪ i ɾ aː ʃ i j i l
മനസ്സിലാകത്തക്ക	m a n̪ a s s i l aː k a t̪ t̪ a k k a
മനസ്സിലായോ	m a n̪ a s s i l aː j oː
മനുഷ്യൻ	m a n̪ u ʂ j a n
മനോരഥം	m a n̪ oː ɾ a t̪ʰ a m
മന്നന്റെ	m a n̪ n̪ a n ṯ e
മരുന്നു	m a ɾ u n̪ n̪ u
മരുമക്കത്തായ	m a ɾ u m a k k a t̪ t̪ aː j a
മര്യാദക്കാരും	m a ɾ j aː d̪ a k k aː ɾ u m
മര്യാദയ്ക്കാണ്	m a ɾ j aː d̪ a j k k aː ɳ
മലയാളത്തിൽ	m a l a j aː ɭ a t̪ t̪ i l
മഹൻ	m a ɦ a n
മഹാപാപി	m a ɦ aː p aː p i
മറന്നുപോയി	m a r a n̪ n̪ u p oː j i
മറ്റാരും	m a ṯ ṯ aː ɾ u m
മറ്റു	m a ṯ ṯ u
മറ്റേ	m a ṯ ṯ eː
മറ്റേത്	m a ṯ ṯ eː t̪
മാത്രമായി	m aː t̪ ɾ a m aː j i
മാത്രമേ	m aː t̪ ɾ a m eː
മാസത്തോളം	m aː s a t̪ t̪ oː ɭ a m
മാറി	m aː r i
മാറ്റി	m aː ṯ ṯ i
മിതിപ്രഭോ	m i t̪ i p ɾ a bʱ oː
മുഖം	m u kʰ a m
മുതലിൽ	m u t̪ a l i l
മുതൽക്കു	m u t̪ a l k k u
മുതൽക്ക്	m u t̪ a l k k
മുത്തച്ഛനായിരുന്നു	m u t̪ t̪ a t͡ʃ t͡ʃʰ a n̪ aː j i ɾ u n̪ n̪ u
മുത്തച്ഛൻ	m u t̪ t̪ a t͡ʃ t͡ʃʰ a n
മുത്തശ്ശി	m u t̪ t̪ a ʃ ʃ i
മുപ്പതുസർഗം	m u p p a t̪ u s a r ɡ a m
മുമ്പിലത്തെ	m u m p i l a t̪ t̪ e
മുമ്പുതന്നെ	m u m p u t̪ a n̪ n̪ e
മുഴുവനെ	m u ɻ u ʋ a n̪ e
മുഴുവൻ	m u ɻ u ʋ a n
മൂന്നു	m uː n̪ n̪ u
മൃഗങ്ങളായി	m rɨ ɡ a ŋ ŋ a ɭ aː j i
മൃഗത്തിന്റെ	m rɨ ɡ a t̪ t̪ i n ṯ e
മൃഗത്തെയെങ്കിലും	m rɨ ɡ a t̪ t̪ e j e ŋ k i l u m
മേനി	m eː n̪ i
മേലിൽ	m eː l i l
മേൽ	m eː l
മേശപ്പുറത്തു	m eː ʃ a p p u r a t̪ t̪ u
മോതിരം	m oː t̪ i ɾ a m
മോതിരത്തെപ്പറ്റി	m oː t̪ i ɾ a t̪ t̪ e p p a ṯ ṯ i
മോതിരവിരലിന്മേൽ	m oː t̪ i ɾ a ʋ i ɾ a l i n̪ m eː l
മോഹം	m oː ɦ a m
മോഹമില്ലാതെ	m oː ɦ a m i l l aː t̪ e
യാതൊരു	j aː t̪ o ɾ u
യോഗവും	j oː ɡ a ʋ u m
രണ്ടു	ɾ a ɳ ʈ u
രണ്ടും	ɾ a ɳ ʈ u m
രണ്ടുകൈകൊണ്ടും	ɾ a ɳ ʈ u k ai̯ k o ɳ ʈ u m
രണ്ടുവഴിക്കും	ɾ a ɳ ʈ u ʋ a ɻ i k k u m
രസം	ɾ a s a m
രസമായില്ലപോൽ	ɾ a s a m aː j i l l a p oː l
രാജശിക്ഷ	ɾ aː ɟ a ʃ i k ʂ a
രാജ്യത്ത്	ɾ aː ɟ j a t̪ t̪
രാത്രി	ɾ aː t̪ ɾ i
രാത്രിയിൽ	ɾ aː t̪ ɾ i j i l
രാമൻ	ɾ aː m a n
ലുനന്തി	l u n̪ a n̪ t̪ i
വകക്കാര്	ʋ a k a k k aː ɾ
വച്ചിരിക്കുന്നു	ʋ a t͡ʃ t͡ʃ i ɾ i k k u n̪ n̪ u
വച്ചിരുന്ന	ʋ a t͡ʃ t͡ʃ i ɾ u n̪ n̪ a
വച്ചിരുന്നതും	ʋ a t͡ʃ t͡ʃ i ɾ u n̪ n̪ a t̪ u m
വച്ച്	ʋ a t͡ʃ t͡ʃ
വൻതരത്തിൽ	ʋ a n t̪ a ɾ a t̪ t̪ i l
വന്നതിനാൽ	ʋ a n̪ n̪ a t̪ i n̪ aː l
വന്നത്	ʋ a n̪ n̪ a t̪
വന്നിട്ടുള്ളവർ	ʋ a n̪ n̪ i ʈ ʈ u ɭ ɭ a ʋ a r
വന്നിരിക്കുന്നു	ʋ a n̪ n̪ i ɾ i k k u n̪ n̪ u
വന്നിരുന്നു	ʋ a n̪ n̪ i ɾ u n̪ n̪ u
വന്നു	ʋ a n̪ n̪ u
വയസ്സു	ʋ a j a s s u
വരരുതെന്നു	ʋ a ɾ a ɾ u t̪ e n̪ n̪ u
വരുത്തുന്ന	ʋ a ɾ u t̪ t̪ u n̪ n̪ a
വരുവാൻ	ʋ a ɾ u ʋ aː n
വല	ʋ a l a
വലത്തോട്ട്	ʋ a l a t̪ t̪ oː ʈ ʈ
വലയിൽ	ʋ a l a j i l
വലിയ	ʋ a l i j a
വല്ല	ʋ a l l a
വളരെ	ʋ a ɭ a ɾ e
വഴികളും	ʋ a ɻ i k a ɭ u m
വഴിക്കും	ʋ a ɻ i k k u m
വാങ്ങുവാൻ	ʋ aː ŋ ŋ u ʋ aː n
വായിക്കുന്ന	ʋ aː j i k k u n̪ n̪ a
വാസനകൊണ്ട്	ʋ aː s a n̪ a k o ɳ ʈ
വാസനയും	ʋ aː s a n̪ a j u m
വാസനാം	ʋ aː s a n̪ aː m
വാസനാബലം	ʋ aː s a n̪ aː b a l a m
വാസനാവികൃതി	ʋ aː s a n̪ aː ʋ i k rɨ t̪ i
വിചാരിക്കുന്നുള്ളൂ	ʋ i t͡ʃ aː ɾ i k k u n̪ n̪ u ɭ ɭ uː
വിചാരിക്കുമ്പോൾ	ʋ i t͡ʃ aː ɾ i k k u m p oː ɭ
വിചാരിച്ചാണ്	ʋ i t͡ʃ aː ɾ i t͡ʃ t͡ʃ aː ɳ
വിചാരിച്ചു	ʋ i t͡ʃ aː ɾ i t͡ʃ t͡ʃ u
വിചാരിച്ച്	ʋ i t͡ʃ aː ɾ i t͡ʃ t͡ʃ
വിഡ്‌ഢിത്തം	ʋ i ɖ ɖʱ i t̪ t̪ a m
വിഡ്‌ഢിത്തത്തിന്റെ	ʋ i ɖ ɖʱ i t̪ t̪ a t̪ t̪ i n ṯ e
വിഡ്‌ഢിയുണ്ടെങ്കിൽ	ʋ i ɖ ɖʱ i j u ɳ ʈ e ŋ k i l
വിഡ്‌ഢ്യാൻ	ʋ i ɖ ɖʱ j aː n
വിദ്യയും	ʋ i d̪ j a j u m
വിദ്യാഭ്യാസവിഷയത്തിൽ	ʋ i d̪ j aː bʱ j aː s a ʋ i ʂ a j a t̪ t̪ i l
വിധമാണ്	ʋ i d̪ʱ a m aː ɳ
വിധേനയും	ʋ i d̪ʱ eː n̪ a j u m
വിലപിടിച്ച	ʋ i l a p i ʈ i t͡ʃ t͡ʃ a
വിശ്വനാഥദർശനവും	ʋ i ʃ ʋ a n̪ aː t̪ʰ a d̪ a r ʃ a n̪ a ʋ u m
വിഹിതാ	ʋ i ɦ i t̪ aː
വീടായതുകൊണ്ട്	ʋ iː ʈ aː j a t̪ u k o ɳ ʈ
വീട്	ʋ iː ʈ
വീട്ടിലുള്ളവരിൽ	ʋ iː ʈ ʈ i l u ɭ ɭ a ʋ a ɾ i l
വീട്ടിൽനിന്നു	ʋ iː ʈ ʈ i l n̪ i n̪ n̪ u
വീട്ടുന്നതിന്	ʋ iː ʈ ʈ u n̪ n̪ a t̪ i n̪
വെടിവയ്ക്കുവാൻ	ʋ e ʈ i ʋ a j k k u ʋ aː n
വെളുത്തും	ʋ e ɭ u t̪ t̪ u m
വേണ്ടാസനത്തിനു	ʋ eː ɳ ʈ aː s a n̪ a t̪ t̪ i n̪ u
വേണ്ടി	ʋ eː ɳ ʈ i
വേർതിരിവ്	ʋ eː r t̪ i ɾ i ʋ
വേർപെടുത്തുവാൻ	ʋ eː r p e ʈ u t̪ t̪ u ʋ aː n
വേറെ	ʋ eː r e
വേറൊരു	ʋ eː r o ɾ u
വൈകുന്നേരത്തെ	ʋ ai̯ k u n̪ n̪ eː ɾ a t̪ t̪ e
വൈഷമ്യത്തിലും	ʋ ai̯ ʂ a m j a t̪ t̪ i l u m
വൈഷമ്യവും	ʋ ai̯ ʂ a m j a ʋ u m
വ്യത്യാസം	ʋ j a t̪ j aː s a m
വ്യസനമായി	ʋ j a s a n̪ a m aː j i
വ്യാഖ്യാനമുണ്ടെങ്കിൽ	ʋ j aː kʰ j aː n̪ a m u ɳ ʈ e ŋ k i l
വ്യുൽപ്പത്തി	ʋ j u l p p a t̪ t̪ i
വ്യുൽപ്പന്നനായിയെന്ന്	ʋ j u l p p a n̪ n̪ a n̪ aː j i j e n̪ n̪
വ്രതാദയഃ	ʋ ɾ a t̪ aː d̪ a j a
ശട്ടം	ʃ a ʈ ʈ a m
ശബ്‌ദത്തിന്	ʃ a b d̪ a t̪ t̪ i n̪
ശരണം	ʃ a ɾ a ɳ a m
ശരിയായിരിക്കാം	ʃ a ɾ i j aː j i ɾ i k k aː m
ശിക്ഷായോഗ്യന്മാരായി	ʃ i k ʂ aː j oː ɡ j a n̪ m aː ɾ aː j i
ശ്രുതിസ്മൃതിഭ്യാം	ʃ ɾ u t̪ i s m rɨ t̪ i bʱ j aː m
സംഗതി	s a m ɡ a t̪ i
സംഗതിയുണ്ടെന്നു	s a m ɡ a t̪ i j u ɳ ʈ e n̪ n̪ u
സങ്കടം	s a ŋ k a ʈ a m
സങ്കടത്തിന്	s a ŋ k a ʈ a t̪ t̪ i n̪
സങ്കടമാണ്	s a ŋ k a ʈ a m aː ɳ
സഞ്ചരിക്കുകയല്ലേ	s a ɲ t͡ʃ a ɾ i k k u k a j a l l eː
സത്യം	s a t̪ j a m
സന്ധ്യാസമയത്ത്	s a n̪ d̪ʱ j aː s a m a j a t̪ t̪
സംബന്ധിച്ചിടത്തോളം	s a m b a n̪ d̪ʱ i t͡ʃ t͡ʃ i ʈ a t̪ t̪ oː ɭ a m
സമയത്ത്	s a m a j a t̪ t̪
സമീപം	s a m iː p a m
സമ്പാദിച്ചപ്പോഴേക്കും	s a m p aː d̪ i t͡ʃ t͡ʃ a p p oː ɻ eː k k u m
സമ്പാദിച്ചു	s a m p aː d̪ i t͡ʃ t͡ʃ u
സമ്പാദ്യം	s a m p aː d̪ j a m
സമ്മാനവും	s a m m aː n̪ a ʋ u m
സർഗ്ഗം	s a r ɡ ɡ a m
സവ്യസാചിത്വവും	s a ʋ j a s aː t͡ʃ i t̪ ʋ a ʋ u m
സംശയം	s a m ʃ a j a m
സഹപാഠികളിൽ	s a ɦ a p aː ʈʰ i k a ɭ i l
സഹായം	s a ɦ aː j a m
സഹിക്കാവുന്ന	s a ɦ i k k aː ʋ u n̪ n̪ a
സാക്ഷി	s aː k ʂ i
സാധനമായാലേ	s aː d̪ʱ a n̪ a m aː j aː l eː
സാധിക്കുന്നതിന്ന്	s aː d̪ʱ i k k u n̪ n̪ a t̪ i n̪ n̪
സാധിച്ചില്ലെങ്കിൽ	s aː d̪ʱ i t͡ʃ t͡ʃ i l l e ŋ k i l
സാമർഥ്യം	s aː m a r t̪ʰ j a m
സാമർഥ്യമുണ്ടാകണമെങ്കിൽ	s aː m a r t̪ʰ j a m u ɳ ʈ aː k a ɳ a m e ŋ k i l
സാമാനം	s aː m aː n̪ a m
സായ്പിന്റെ	s aː j p i n ṯ e
സൗഖ്യവും	s au̯ kʰ j a ʋ u m
സൗന്ദര്യവും	s au̯ n̪ d̪ a ɾ j a ʋ u m
സ്തംഭാകാരമായിട്ടു	s t̪ a m bʱ aː k aː ɾ a m aː j i ʈ ʈ u
സ്ഥലത്താണെന്നു	s t̪ʰ a l a t̪ t̪ aː ɳ e n̪ n̪ u
സ്‌നേഹിതയായ	s n̪ eː ɦ i t̪ a j aː j a
സ്വപ്നം	s ʋ a p n̪ a m
സ്റ്റേഷനിൽ	s ṯ ṯ eː ʂ a n̪ i l

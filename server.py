from flask import Flask, render_template, redirect, url_for, flash, request
from werkzeug.utils import secure_filename
import os
import subprocess
# import glob


APPNAME ="Malayalam Speech Recognition"
app = Flask(__name__) 
UPLOAD_FOLDER = './uploads'		
ALLOWED_EXTENSIONS = {'wav','webm'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
           
@app.route('/') 
def index(): 
    return render_template('index.html', name=APPNAME)

@app.route('/api/recognize', methods=['POST']) 
def recognize(): 
    file= request.files['audio']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file :
        if allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filePath=os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filePath)
            print('Saved the file at ', filePath)
            process = subprocess.Popen(['bash', './quick-asr.sh', filePath ])
            wait_status =  process.wait()
            process.kill()
            resultfile = open("./transcriptions/n-best-hypothesis.txt", encoding="utf-8")
            resultlines = resultfile.readlines()
            result = ''
            # Strips the newline character 
            for line in resultlines:
                result = result + line.split(' ',1)[1]
            os.remove(filePath)
        else :
            result="Please upload audio in wav format. Got " + file.filename
    return {
        "text":  result
    }


# main driver function 
if __name__ == '__main__': 
    app.run(host='0.0.0.0', port=8080, ssl_context='adhoc') 
 

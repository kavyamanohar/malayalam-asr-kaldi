 
#set-up for single machine or cluster based execution
. ./cmd.sh
#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh


train_dict=dict_mlphon
train_lang=lang_model_mlphon
exp=exp_mlphon
nj=6


echo "===== MONO GRAPH ====="
echo

# utils/mkgraph.sh --mono data/$train_lang $exp/mono $exp/mono/graph || exit 1


# echo "===== TRI1 GRAPH ====="
# echo
# utils/mkgraph.sh data/$train_lang $exp/tri_200_16000 $exp/tri_200_16000/graph || exit 1



# echo "===== TRI_LDA GRAPH ====="
# echo

utils/mkgraph.sh data/$train_lang $exp/tri_400_17000_lda $exp/tri_400_17000_lda/graph 


# echo "===== TRI_SAT GRAPH ====="

# utils/mkgraph.sh data/$train_lang exp/tri_sat exp/tri_sat/graph 


echo ============================================================================
echo "                   End of Script             	        "
echo ============================================================================

# This script transcribes a wavefile sampled at 16 kHz, result will be stored in /transcription/one-best-hypothesis.txt
# This script is based on the blogpost by  Josh Meyer: http://jrmeyer.github.io/asr/2016/09/12/Using-built-GMM-model-Kaldi.html

#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh

echo "Input Audio Preparation"
rm -rf ./inputaudio/transcriptions

mkdir ./inputaudio/transcriptions

realpath ./inputaudio/*.wav > ./inputaudio/transcriptions/wavefilepaths.txt


echo "Creating the list of utterence IDs"

# The function is to extract the utterance id
cat ./inputaudio/transcriptions/wavefilepaths.txt | xargs -l basename -s .wav > ./inputaudio/transcriptions/utt


echo "Creating the list of utterence IDs mapped to absolute file paths of wavefiles"


#Create wav.scp mapping from uttrence id to absolute wave file paths
paste ./inputaudio/transcriptions/utt ./inputaudio/transcriptions/wavefilepaths.txt > ./inputaudio/transcriptions/wav.scp

rm ./inputaudio/transcriptions/utt
rm ./inputaudio/transcriptions/wavefilepaths.txt

echo "=================COMPUTING MFCC====================="

#subtract-mean = true in the following line is equivalent to mean variance normalization during training

compute-mfcc-feats \
    --config=conf/mfcc.conf \
    --subtract-mean=true \
    --allow-upsample=true \
    scp:./inputaudio/transcriptions/wav.scp \
    ark,scp:./inputaudio/transcriptions/feats.ark,./inputaudio/transcriptions/feats.scp 

# add-deltas \
#     scp:./inputaudio/transcriptions/feats.scp \
#     ark,scp:./inputaudio/transcriptions/delta-feats.ark,./inputaudio/transcriptions/delta-feats.scp


splice-feats --left-context=2 --right-context=2 \
    scp:./inputaudio/transcriptions/feats.scp \
    ark,scp:./inputaudio/transcriptions/splice-feats.ark,./inputaudio/transcriptions/splice-feats.scp

# transform-feats \
#     ./exp/tri_lda/final.mat \
#     scp:./inputaudio/transcriptions/splice-feats.scp \
#     ark,scp:./inputaudio/transcriptions/lda-feats.ark,./inputaudio/transcriptions/lda-feats.scp

transform-feats \
    ./exp/tri_lda/final.mat \
    scp:./inputaudio/transcriptions/splice-feats.scp \
    ark,scp:./inputaudio/transcriptions/lda-feats.ark,./inputaudio/transcriptions/lda-feats.scp



# ali-to-post "ark:gunzip -c exp/tri_lda_ali/ali.1.gz|" ark:- | weight-silence-post 0.0 1:2:3:4:5:6:7:8:9:10 exp/tri_lda_ali/final.mdl ark:- ark:- | gmm-est-fmllr --fmllr-update-type=full --spk2utt=ark:data/train2/split5/1/spk2utt exp/tri_lda_ali/final.mdl "ark,s,cs:apply-cmvn  --utt2spk=ark:data/train2/split5/1/utt2spk scp:data/train2/split5/1/cmvn.scp scp:data/train2/split5/1/feats.scp ark:- | splice-feats --left-context=2 --right-context=2 ark:- ark:- | transform-feats exp/tri_lda_ali/final.mat ark:- ark:- |" ark:- ark:exp/tri_sat/trans.1 

# feat-to-dim scp:./inputaudio/transcriptions/feats.scp
echo "==============Make Graph=============="
# FIXME move to training
#utils/mkgraph.sh --mono data/lang_bigram exp/mono exp/mono/graph || exit 1
#utils/mkgraph.sh data/lang_bigram exp/tri1 exp/tri1/graph || exit 1



echo "GMM-HMM + FEATURE VECTOR ======> LATTICE"

# Use appropriate decoding graphs: mono, tri1, tri_lda, tri_sat. But you have to make
# sure the audio processing should have been done accordingly. 


usegraph=tri_lda


# gmm-latgen-faster \
#     --max-active=7000 --beam=11.0 --lattice-beam=6.0 --acoustic-scale=0.083333 --allow-partial=true \
#     --word-symbol-table=exp/$usegraph/graph/words.txt \
#     exp/$usegraph/final.mdl \
#     exp/$usegraph/graph/HCLG.fst \
#     ark:./inputaudio/transcriptions/lda-feats.ark \
#     ark,t:./inputaudio/transcriptions/lattices.ark


# nnet-latgen-faster --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true --word-symbol-table=exp/tri_sat/graph/words.txt exp/DNN_tri_sat_aligned_layer3_nodes256/30.mdl exp/tri_sat/graph/HCLG.fst 'ark,s,cs:apply-cmvn  --utt2spk=ark:data/test2/split5/1/utt2spk scp:data/test2/split5/1/cmvn.scp scp:data/test2/split5/1/feats.scp ark:- | splice-feats --left-context=2 --right-context=2 ark:- ark:- | transform-feats exp/DNN_tri_sat_aligned_layer3_nodes256/final.mat ark:- ark:- |' 'ark:|gzip -c > exp/DNN_tri_sat_aligned_layer3_nodes256/decode/lat.1.gz' 
# transform-feats exp/DNN_tri_sat_aligned_layer3_nodes256/final.mat ark:- ark:- 
# splice-feats --left-context=2 --right-context=2 ark:- ark:- 


nnet-latgen-faster --verbose=2\
    --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true \
    --word-symbol-table=exp/$usegraph/graph/words.txt \
    exp/DNN_tri_lda_aligned_layer3_nodes256/final.mdl \
    exp/$usegraph/graph/HCLG.fst \
    ark:./inputaudio/transcriptions/lda-feats.ark \
    ark,t:./inputaudio/transcriptions/lattices.ark

echo "ONE BEST LATTICE"

lattice-best-path \
     --lm-scale=10 \
    --word-symbol-table=exp/$usegraph/graph/words.txt \
    ark:./inputaudio/transcriptions/lattices.ark \
    ark,t:./inputaudio/transcriptions/one-best.tra

echo "ONE BEST WORD SEQUENCE"

utils/int2sym.pl -f 2- \
    exp/$usegraph/graph/words.txt \
    ./inputaudio/transcriptions/one-best.tra \
    > ./inputaudio/transcriptions/one-best-hypothesis.txt

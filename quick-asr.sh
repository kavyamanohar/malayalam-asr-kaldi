# This script transcribes a wavefile sampled at 16 kHz, result will be stored in /transcription/one-best-hypothesis.txt
# This script is based on the blogpost by  Josh Meyer: http://jrmeyer.github.io/asr/2016/09/12/Using-built-GMM-model-Kaldi.html

#set the paths to binaries and other executables
[ -f path.sh ] && . ./path.sh

echo "Input Audio Preparation"
rm -rf ./transcriptions

mkdir ./transcriptions

ffmpeg -i $1 -y $1.wav
echo $1.wav > ./transcriptions/wavefilepaths.txt


echo "Creating the list of utterence IDs"

# The function is to extract the utterance id
cat ./transcriptions/wavefilepaths.txt | xargs -l basename -s .wav > ./transcriptions/utt


echo "Creating the list of utterence IDs mapped to absolute file paths of wavefiles"


#Create wav.scp mapping from uttrence id to absolute wave file paths
paste ./transcriptions/utt ./transcriptions/wavefilepaths.txt > ./transcriptions/wav.scp

# rm ./inputaudio/transcriptions/utt
# rm ./inputaudio/transcriptions/wavefilepaths.txt

echo "=================COMPUTING MFCC====================="

#subtract-mean = true in the following line is equivalent to mean variance normalization during training

compute-mfcc-feats \
    --config=conf/mfcc.conf \
    --subtract-mean=true \
    --allow-upsample=true \
    scp:./transcriptions/wav.scp \
    ark,scp:./transcriptions/feats.ark,./transcriptions/feats.scp 

splice-feats --left-context=2 --right-context=2 \
    scp:./transcriptions/feats.scp \
    ark,scp:./transcriptions/splice-feats.ark,./transcriptions/splice-feats.scp

transform-feats \
    ./exp_mlphon/tri_400_17000_lda/final.mat \
    scp:./transcriptions/splice-feats.scp \
    ark,scp:./transcriptions/lda-feats.ark,./transcriptions/lda-feats.scp

echo "==============Make Graph=============="
# FIXME move to training
#utils/mkgraph.sh --mono data/lang_bigram exp/mono exp/mono/graph || exit 1
#utils/mkgraph.sh data/lang_bigram exp/tri1 exp/tri1/graph || exit 1



usegraph=tri_400_17000_lda


nnet-latgen-faster --verbose=2\
    --minimize=false --max-active=7000 --min-active=200 --beam=15.0 --lattice-beam=8.0 --acoustic-scale=0.1 --allow-partial=true \
    --word-symbol-table=exp_mlphon/$usegraph/graph/words.txt \
    exp_mlphon/DNN_tri_lda_aligned_highlr_3_256_10/final.mdl \
    exp_mlphon/$usegraph/graph/HCLG.fst \
    ark:./transcriptions/lda-feats.ark \
    ark,t:./transcriptions/lattices.ark

echo "ONE BEST LATTICE"

lattice-best-path \
     --lm-scale=10  \
    --word-symbol-table=exp_mlphon/$usegraph/graph/words.txt \
    ark:./transcriptions/lattices.ark \
    ark,t:./transcriptions/one-best.tra \
    ark:./transcriptions/out.ali

echo "ONE BEST WORD SEQUENCE"

utils/int2sym.pl -f 2- \
    exp_mlphon/$usegraph/graph/words.txt \
    ./transcriptions/one-best.tra \
    > ./transcriptions/one-best-hypothesis.txt

cat ./transcriptions/one-best-hypothesis.txt


echo "N BEST LATTICE"

lattice-to-nbest --n=10 --acoustic-scale=0.1 ark:./transcriptions/lattices.ark ark,t:./transcriptions/nbest.ark
nbest-to-linear ark:./transcriptions/nbest.ark ark:./transcriptions/ark:1.ali ark,t:./transcriptions/nbestwords.int ark:./transcriptions/1.lmscore ark:./transcriptions/1.acscore
utils/int2sym.pl -f 2- \
    exp_mlphon/$usegraph/graph/words.txt \
    ./transcriptions/nbestwords.int \
    > ./transcriptions/n-best-hypothesis.txt
cat ./transcriptions/n-best-hypothesis.txt


rm -f $1.wav


echo "Lattice FST Structure"

# utterance=$( cat ./transcriptions/utt )

# gzip -c ./transcriptions/lattices.ark >> ./transcriptions/lat.gz
# utils/show_lattice.sh $utterance ./transcriptions/lat.gz exp_mlphon/tri_400_17000_lda/graph/words.txt